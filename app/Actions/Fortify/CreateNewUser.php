<?php

namespace App\Actions\Fortify;

use App\Models\User;
use App\Services\ChamiloAccountService;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;
use Laravel\Fortify\Contracts\CreatesNewUsers;

class CreateNewUser implements CreatesNewUsers
{
    use PasswordValidationRules;

    protected $chamiloAccountService;
    /**
     * UsersController constructor.
     * @param ChamiloAccountService $chamiloAccountService
     */
    public function __construct(ChamiloAccountService $chamiloAccountService)
    {
        $this->chamiloAccountService = $chamiloAccountService;
    }

    /**
     * @param array $input
     * @return mixed
     * @throws \App\Tools\SoapWrapper\Exceptions\ServiceAlreadyExists
     * @throws \Illuminate\Validation\ValidationException
     */
    public function create(array $input)
    {
        Validator::make($input, [
            'gender' => ['required', 'string', 'max:255'],
            'name' => ['required', 'string', 'max:255'],
            'surname' => ['required', 'string','max:255'],
            'dad_name' => ['required', 'string','max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'specialty' => ['required', 'string', 'max:255'],
            'password' => $this->passwordRules(),

        ])->validate();

        $user = User::create([
            'gender' => $input['gender'],
            'name' => $input['name'],
            'surname' => $input['surname'],
            'dad_name' => $input['dad_name'],
            'email' => $input['email'],
            'specialty' => $input['specialty'],
            'password' => Hash::make($input['password']),
            'username' => $this->getUsername($input['email'])
        ]);

        //Attach student role to the new user
        $user->roles()->attach(2);
        $userId = (int) $this->chamiloAccountService->create('student',$user);
        $user->chamilo_id = $userId;
        $user->save();
        return $user;
    }

    /**
     * @param $email
     * @return mixed|string
     */
    private function getUsername($email){
        $username = explode("@",$email)[0];
        $userWithSameUsernameExists = User::where('username',$username)->exists();
        $counter = 1;
        while($userWithSameUsernameExists){
            $username .= $counter;
            $counter++;
            $userWithSameUsernameExists = User::where('username',$username)->exists();
        }
        return $username;
    }

}
