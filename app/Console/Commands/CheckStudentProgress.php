<?php

namespace App\Console\Commands;

use App\Jobs\GenerateCertificate;
use App\Models\Application;
use App\Services\ChamiloCourseService;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Log;

class CheckStudentProgress extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:student_progress';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    protected $chamiloCourseService;


    /**
     * Create a new command instance.
     * @param ChamiloCourseService $chamiloCourseService
     * @return void
     */

    public function __construct(ChamiloCourseService $chamiloCourseService)
    {
        $this->chamiloCourseService = $chamiloCourseService;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $applicationsToCheck = Application::whereHas('status',function($statusQuery){
            $statusQuery->where('application_statuses.name','approved');
        })->whereHas('course',function($courseQuery){
            $courseQuery->where('courses.course_end','>',Carbon::today());
        })->where('certified',false)->get();

        foreach($applicationsToCheck as $application){
            $response = $this->chamiloCourseService
                ->courseProgressTest($application->user->chamilo_id,$application->course->code);

            if(!isset($response->error)){
                if($response->max_score >= $response->min_score){
                    $applicationUpdateParameters = ['certified'=>true];
                    if($application->course->free){
                        GenerateCertificate::dispatch($application)->onQueue('low');
                    }
                    $application->update($applicationUpdateParameters);
                }
            }
        }
        return 0;
    }
}
