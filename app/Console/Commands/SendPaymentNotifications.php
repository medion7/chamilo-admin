<?php

namespace App\Console\Commands;

use App\Mail\Students\PaymentNotification;
use App\Models\Application;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class SendPaymentNotifications extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:payment_notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $applicationsToNotify = Application::whereHas('payments',function($paymentQuery){
            $paymentQuery->where('notified',false)->whereHas('schedule',function($paymentScheduleQuery){
                $paymentScheduleQuery->where('payment_date',Carbon::today()->addDays(10));
            });
        })->get();
        foreach($applicationsToNotify as $application){
            $payment = $application->payments()->where('notified',false)->whereHas('schedule',function($paymentScheduleQuery){
                $paymentScheduleQuery->where('payment_date',Carbon::today()->addDays(10));
            })->first();
            $payment->update(['notified'=>true]);
            Mail::to($application->user)->send(new PaymentNotification($payment));
        }
        return 0;
    }
}
