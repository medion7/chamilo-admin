<?php

namespace App\Http\Controllers\Admin;

use App\Events\ApplicationApproved;
use App\Models\Application;
use App\Http\Controllers\Controller;
use App\Models\ApplicationStatus;
use App\Services\ChamiloCourseService;
use App\Traits\tableHelpers;

class ApplicationsController extends Controller
{
    use tableHelpers;

    protected $chamiloCourseService;
    /**
     * UsersController constructor.
     * @param ChamiloCourseService $chamiloCourseService
     */
    public function __construct(ChamiloCourseService $chamiloCourseService)
    {
        $this->chamiloCourseService = $chamiloCourseService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index()
    {
        $applicationsBaseQuery = Application::with(['status', 'user', 'course']);
        $this->filter($applicationsBaseQuery);
        $this->search($applicationsBaseQuery);
        return view('admin.applications.index', [
            'applications' => $applicationsBaseQuery->orderBy('status_id')->paginate(10),
            'applicationStatuses' => ApplicationStatus::all()
        ]);
    }

    /**
     * @param $applicationsBaseQuery
     */
    protected function filter($applicationsBaseQuery){
        if(request()->has('status') && !empty(request()->input('status'))){
            $applicationsBaseQuery->whereHas('status',function($statusQuery){
               $statusQuery->where('name',request()->input('status'));
            });
        }
    }

    /**
     * @param $applicationsBaseQuery
     */
    protected function search($applicationsBaseQuery){
        $searchableColumnsOnApplications = [];
        $relationsMapping = [
            'user' => ['id', 'name', 'surname', 'email'],
            'course' => ['title']
        ];
        if (request()->has('term') && !empty(request()->input('term'))) {
            $term = request()->input('term');
            $this->applySearch($searchableColumnsOnApplications,$relationsMapping,$applicationsBaseQuery,$term);
        }
    }

    /**
     * @param Application $application
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve(Application $application)
    {
        $applicationStatus = ApplicationStatus::whereName('approved')->first();
        $application->update([
            'status_id' => $applicationStatus->id
        ]);
        if(!$application->course->free) {
            foreach ($application->course->paymentSchedules as $paymentSchedule) {
                $payment = $application->payments()->make([
                    'schedule_id' => $paymentSchedule->id,
                ]);
                $payment->save();
            }
        }
        ApplicationApproved::dispatch($application);
        $this->chamiloCourseService->subscribe($application->course,$application->user);
        return response()->json(['message' => 'Η αίτηση εγκρίθηκε']);
    }

    /**
     * @param Application $application
     * @return \Illuminate\Http\JsonResponse
     */
    public function reject(Application $application)
    {
        $applicationStatus = ApplicationStatus::whereName('rejected')->first();
        $application->update([
            'status_id' => $applicationStatus->id
        ]);
        return response()->json(['message' => 'Η αίτηση απορρίφθηκε']);
    }
}

