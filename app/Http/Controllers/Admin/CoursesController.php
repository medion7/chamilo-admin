<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\Courses\UpdateImageRequest;
use App\Models\Application;
use App\Models\Course;
use App\Http\Controllers\Controller;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Traits\tableHelpers;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class CoursesController extends Controller
{
    use tableHelpers;

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $coursesBaseQuery = Course::select();
        $this->search($coursesBaseQuery);
        return view('admin.courses.index', [
            'courses' => $coursesBaseQuery->paginate(10)
        ]);
    }

    /**
     * @param $coursesBaseQuery
     */
    protected function search($coursesBaseQuery){
        $searchableColumnsOnCourses = ['title','body','price','code'];
        $relationsMapping = [
        ];
        if (request()->has('term') && !empty(request()->input('term'))) {
            $term = request()->input('term');
            $this->applySearch($searchableColumnsOnCourses,$relationsMapping,$coursesBaseQuery,$term);
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin.courses.add');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(Course $course)
    {
        return view('admin.courses.edit',(
            [
                'course'=> $course
            ]
        ));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, Course $course)
    {
        $this->validate($request, [
            'title' => 'required',
            'price' => 'required|integer',
            'code' => 'required',
            'course_start' => 'required|date_format:d/m/Y',
            'course_end' => 'required|date_format:d/m/Y',
            'registration_start' => 'required|date_format:d/m/Y',
            'registration_end' => 'required|date_format:d/m/Y',
        ]);
        $course->title = $request->input('title');
        $course->body = $request->input('body');
        $course->certificate_description = $request->input('certificate_description');
        $course->code = $request->input('code');
        $course->program_code = $request->input('program_code');
        $course->free = $request->input('free');
        $course->course_start = Carbon::createFromFormat('d/m/Y', $request->input('course_start'));
        $course->course_end = Carbon::createFromFormat('d/m/Y', $request->input('course_end'));
        $course->price = $request->input('price');
        $course->registration_start = Carbon::createFromFormat('d/m/Y', $request->input('registration_start'));
        $course->registration_end = Carbon::createFromFormat('d/m/Y', $request->input('registration_end'));
        $course->save();
        return redirect()->route('admin.courses.index')->with('success', 'Το μάθημα αποθηκεύτηκε');
    }

    /**
     * @param UpdateImageRequest $updateImageRequest
     * @param Course $course
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateImage(UpdateImageRequest $updateImageRequest,Course $course){
        $path = $updateImageRequest->file('file')->store(
            'courses',
            'public'
        );
        $course->update(['photo_path' => $path]);
        return response()->json(['image uploaded']);
    }

    /**
     * @param Course $course
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteImage(Course $course){
        Storage::disk('public')->delete($course->photo_path);
        $course->update(['photo_path' => null]);
        return response()->json(['image deleted']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'price' => 'required|integer',
            'code' => 'required',
            'course_start' => 'required|date_format:d/m/Y',
            'course_end' => 'required|date_format:d/m/Y',
            'registration_start' => 'required|date_format:d/m/Y',
            'registration_end' => 'required|date_format:d/m/Y',
        ]);

        //      New Course
        $course = new Course();
        $course->title = $request->input('title');
        $course->price = $request->input('price');
        $course->body = $request->input('body');
        $course->certificate_description = $request->input('certificate_description');
        $course->code = $request->input('code');
        $course->free = $request->input('free');
        $course->program_code = $request->input('program_code');
        $course->course_start = Carbon::createFromFormat('d/m/Y',$request->input('course_start'));
        $course->course_end = Carbon::createFromFormat('d/m/Y',$request->input('course_end'));
        $course->registration_start = Carbon::createFromFormat('d/m/Y',$request->input('registration_start'));
        $course->registration_end = Carbon::createFromFormat('d/m/Y',$request->input('registration_end'));
        $course->save();
        return redirect()->route('admin.courses.index')->with('success', 'Το μάθημα προστέθηκε.');
    }

    /**
     * @param Course $course
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Course $course)
    {
        $course->delete();
        return response()->json(['message' => 'Το μάθημα διαγράφηκε.']);
    }

    /**
     * @param Course $course
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function students(Course $course){
        $baseQuery = User::ofRole('student')->with(['applications'=>function($applicationQuery) use ($course){
            $applicationQuery->approved()->where('course_id',$course->id);
        }]);
        $this->searchStudents($baseQuery);
        return view('admin.students.index',[
            'course' => $course,
            'students' => $baseQuery->whereHas('applications',function($applicationQuery) use ($course){
                $applicationQuery->approved()->where('course_id',$course->id);
            })->paginate(10)
        ]);
    }


    /**
     * @param $usersBaseQuery
     */
    protected function searchStudents($usersBaseQuery){
        $searchableColumnsOnCourses = ['name','surname','dad_name','email','username'];
        $relationsMapping = [
        ];
        if (request()->has('term') && !empty(request()->input('term'))) {
            $term = request()->input('term');
            $this->applySearch($searchableColumnsOnCourses,$relationsMapping,$usersBaseQuery,$term);
        }
    }

    /**
     * @param Course $course
     * @return \Illuminate\Http\JsonResponse
     */
    public function createCertificates(Course $course){
        $applications = Application::where('course_id',$course->id)
            ->approved()
            ->where('certified',true)
            ->whereDoesntHave('certificate')
            ->get();

        foreach($applications as $application){
            $application->generateCertificate();
        }
        return response()->json(['message' => 'Τα πιστοποιητικά δημιουργήθηκαν.']);
    }
}

