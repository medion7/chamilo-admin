<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Course;
use App\Models\PaymentSchedule;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PaymentSchedulesController extends Controller
{

    /**
     * @param Course $course
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function index(Course $course){
        if($course->free){
            return back();
        }
        return view('admin.payment_schedules.index',[
            'course' => $course
        ]);
    }

    /**
     * @param Course $course
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function create(Course $course){
        return view('admin.payment_schedules.add',[
            'course' => $course
        ]);
    }

    /**
     * @param Course $course
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Course $course,Request $request){
        $this->validate($request, [
            'amount' => 'required|numeric',
            'payment_date' => 'required|date_format:d/m/Y',
        ]);
        $course->paymentSchedules()->create([
            'amount' => $request->input('amount'),
            'payment_date' => Carbon::createFromFormat('d/m/Y',$request->input('payment_date')),
        ]);
        return redirect()->route('admin.courses.payment-schedules.index',$course->id)
            ->with('success', 'Η δόση πληρωμής προστέθηκε');
    }

    /**
     * @param Course $course
     * @param PaymentSchedule $paymentSchedule
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit(Course $course,PaymentSchedule $paymentSchedule){
        return view('admin.payment_schedules.edit',[
            'course' => $course,
            'payment_schedule'=> $paymentSchedule
        ]);
    }

    /**
     * @param Course $course
     * @param PaymentSchedule $paymentSchedule
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Course $course,PaymentSchedule $paymentSchedule, Request $request){
        $this->validate($request, [
            'amount' => 'required|numeric',
            'payment_date' => 'required|date_format:d/m/Y',
        ]);
        $paymentSchedule->update([
            'amount' => $request->input('amount'),
            'payment_date' => Carbon::createFromFormat('d/m/Y',$request->input('payment_date')),
        ]);
        return redirect()->route('admin.courses.payment-schedules.index',$course->id)
            ->with('success', 'Η Δόση Πληρωμής αποθηκεύτηκε');
    }


    /**
     * @param Course $course
     * @param PaymentSchedule $paymentSchedule
     * @return \Illuminate\Http\JsonResponse
    */
    public function delete(Course $course,PaymentSchedule $paymentSchedule){

        $paymentSchedule->delete();
        return response()->json(['message' => 'Η Δόση Πληρωμής διαγράφηκε']);
    }

}
