<?php

namespace App\Http\Controllers\Admin;

use App\Events\PaymentDocumentApproved;
use App\Events\PaymentDocumentRejected;
use App\Http\Controllers\Controller;
use App\Models\Application;
use App\Models\Payment;
use App\Models\PaymentStatus;
use Illuminate\Support\Facades\Storage;

class PaymentsController extends Controller
{
    /**
     * @param Application $application
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Application $application){
        return view('admin.payments.index',[
            'payments' => $application->payments()->with(['status','schedule'])->paginate(10),
            'application' => $application->load('user')
        ]);
    }

    /**
     * @param Application $application
     * @param Payment $payment
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function downloadDocument(Application $application, Payment $payment){
        return Storage::disk('local')->download($payment->invoice->path);
    }

    /**
     * @param Application $application
     * @param Payment $payment
     * @return \Illuminate\Http\JsonResponse
     */
    public function approve(Application $application,Payment $payment){
        if($payment->status->name === 'pending'){
            $paymentStatus = PaymentStatus::whereName('approved')->first();
            $payment->update(['status_id'=>$paymentStatus->id]);
            PaymentDocumentApproved::dispatch($payment);
            return response()->json(['message' => 'Η πληρωμή εγκρίθηκε']);
        }else{
            return response()->json(['message' => 'Η πληρωμή δεν εγκρίθηκε, γιατί δεν βρίσκεται σε κατάσταση Pending']);
        }
    }

    /**
     * @param Application $application
     * @param Payment $payment
     * @return \Illuminate\Http\JsonResponse
     */
    public function reject(Application $application,Payment $payment){
        if($payment->status->name === 'pending') {
            $paymentStatus = PaymentStatus::whereName('rejected')->first();
            $payment->update(['status_id' => $paymentStatus->id]);
            PaymentDocumentRejected::dispatch($payment);
            return response()->json(['message' => 'Η πληρωμή απορρίφθηκε']);
        }else{
            return response()->json(['message' => 'Η πληρωμή δεν απορρίφθηκε, γιατί δεν βρίσκεται σε κατάσταση Pending']);
        }
    }
}
