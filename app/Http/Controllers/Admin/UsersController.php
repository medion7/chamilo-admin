<?php

namespace App\Http\Controllers\Admin;

use App\Models\Role;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Services\ChamiloCourseService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Traits\tableHelpers;

class UsersController extends Controller

{
    use tableHelpers;

    protected $chamiloCourseService;
    /**
     * UsersController constructor.
     * @param ChamiloCourseService $chamiloCourseService
     */
    public function __construct(ChamiloCourseService $chamiloCourseService)
    {
        $this->chamiloCourseService = $chamiloCourseService;
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $usersBaseQuery = User::with('roles');
        $this->search($usersBaseQuery);
        return view('admin.users.index', [
            'users' => $usersBaseQuery->paginate(10)
        ]);
    }

    /**
     * @param $usersBaseQuery
     */
    protected function search($usersBaseQuery){
        $searchableColumnsOnCourses = ['name','surname','dad_name','email','username'];
        $relationsMapping = [
        ];
        if (request()->has('term') && !empty(request()->input('term'))) {
            $term = request()->input('term');
            $this->applySearch($searchableColumnsOnCourses,$relationsMapping,$usersBaseQuery,$term);
        }
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        $roles = Role::get();
        return view('admin.users.add',
            [
                'roles' => $roles
            ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        $user = User::find($id);
        $roles = Role::get();
        return view('admin.users.edit',(
            [
                'roles' => $roles,
                'user' => $user,
            ]));
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        if (Auth::user()->email === $request['email']) {
            $this->validate($request, [
                'name' => 'required',
                'surname' => 'required',
                'dad_name' => 'required',
                'specialty' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'role_id' => 'required'

            ]);
        } else {
            $this->validate($request, [
                'name' => 'required',
                'surname' => 'required',
                'dad_name' => 'required',
                'specialty' => 'required',
                'email' => 'required|email',
                'password' => 'required',
                'role_id' => 'required'

            ]);
        }

        $user = User::find($id);
        $user->name = $request->get('name');
        $user->surname = $request->get('surname');
        $user->dad_name = $request->get('dad_name');
        $user->specialty = $request->get('specialty');
        $user->email = $request->get('email');
        $user->password = Hash::make($request->get('password'));
        $user->roles()->attach($request->get('role_id'));
        $user->save();
        return redirect()->route('admin.users.index')->with('success', 'Οι αλλαγές έγιναν επιτυχώς!');
    }



    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'surname' => 'required',
            'dad_name' => 'required',
            'specialty' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required',
            'role_id' => 'required'
        ]);
//      New User
        $user = new User;
        $user->name = $request->input('name');
        $user->surname = $request->input('surname');
        $user->dad_name = $request->input('dad_name');
        $user->specialty = $request->input('specialty');
        $user->email = $request->input('email');
        $user->password = Hash::make($request->input('password'));
        $user->username = $this->getUsername($request['email']);
        $user->save();
        $user->roles()->attach($request->input('role_id'));
        return redirect()->route('admin.users.index')->with('success', 'Ο χρήστης προστέθηκε');
    }

    /**
     * @param $email
     * @return mixed|string
     */
    private function getUsername($email){
        $username = explode("@",$email)[0];
        $userWithSameUsernameExists = User::where('username',$username)->exists();
        $counter = 1;
        while($userWithSameUsernameExists){
            $username .= $counter;
            $counter++;
            $userWithSameUsernameExists = User::where('username',$username)->exists();
        }
        return $username;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete($id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(['message' => 'Ο Χρήστης διαγράφηκε']);
    }
}
