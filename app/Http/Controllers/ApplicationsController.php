<?php

namespace App\Http\Controllers;

use App\Events\ApplicationCreated;
use App\Models\Application;
use App\Models\ApplicationStatus;
use App\Models\Course;
use Illuminate\Support\Facades\Storage;


class ApplicationsController extends Controller
{
    /**
     * @param Course $course
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Course $course)
    {
        $user = request()->user();
        $pendingStatus = ApplicationStatus::where('name','pending')->first();
        $application = $user->applications()->create([
            'course_id' => $course->id,
            'status_id' => $pendingStatus->id,
            'requires_invoice' => request()->has('requires_invoice')
        ]);

        // Trigger application created event
        ApplicationCreated::dispatch($application);
        return back()->with('status','Η δήλωση σας ήταν επιτυχής');
    }

    /**
     * @param Course $course
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function showCourse(Course $course){
        return view('apply-for-course',[
            'course' => $course,
        ]);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function accept()
    {
        return back()->with('Status Changed');
    }

    /**
     * @param Application $application
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function downloadCertificate(Application $application){
        if($application->certified && $application->certificate){
            return Storage::disk('local')->download($application->certificate->path);
        }
    }
}
