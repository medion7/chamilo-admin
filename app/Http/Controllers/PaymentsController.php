<?php

namespace App\Http\Controllers;

use App\Events\PaymentDocumentUploaded;
use App\Http\Requests\Payments\UploadDocumentRequest;
use App\Models\Application;
use App\Models\Document;
use App\Models\DocumentType;
use App\Models\Payment;
use App\Models\PaymentStatus;
use Carbon\Carbon;

class PaymentsController extends Controller
{
    /**
     * @param Application $application
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function index(Application $application){
        $application->load('course');
        $payments = $application->payments()->select('payments.*')->with(['schedule','status'])
        ->join('payment_schedules','payment_schedules.id','=','payments.schedule_id')
        ->orderBy('payment_schedules.payment_date')
        ->get();

        return view('applications.payments',[
            'payments' => $payments,
            'application' => $application
        ]);
    }

    /**
     * @param UploadDocumentRequest $uploadDocumentRequest
     * @param Payment $payment
     * @return \Illuminate\Http\RedirectResponse
     */
    public function uploadDocument(UploadDocumentRequest $uploadDocumentRequest, Payment $payment){
        $documentType = DocumentType::where('name','invoice')->first();
        $invoicePath = $uploadDocumentRequest->file('document')->store('invoices');
        $payment->invoice()->create(
            [
                'type_id' => $documentType->id,
                'path' => $invoicePath
            ]
        );
        $paymentStatus = PaymentStatus::whereName('pending')->first();
        $payment->update([
           'payment_date' => Carbon::now(),
           'status_id' => $paymentStatus->id
        ]);
        PaymentDocumentUploaded::dispatch($payment);
        return back()->with('status','Το αποδεικτικό ανέβηκε');
    }


}
