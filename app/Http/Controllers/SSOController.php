<?php

namespace App\Http\Controllers;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class SSOController extends Controller
{

    /**
     * This method is responsible for authenticating the user to the various chamilo applications
     * @param Request $request
     * @return RedirectResponse|Redirector
     */
    public function sso_callback(Request $request)
    {
        if(!$request->has('sso_referer') || !$request->has('sso_challenge')){
            return redirect()->route('home');
        }
        $sso_challenge = $request->sso_challenge;
        $sso_referer = $request->sso_referer;
        $target_host = urldecode($sso_referer);
        $host = $request->getHttpHost();
        $host_with_protocol = url('/');
        $user = Auth::user();
        //Need to find appropriate key & host for each different chamilo sso target
        $master_auth_uri = '/sso_callback';

        $key = env('CHAMILO_APP_KEY');

        /**
         * Master Cookie
         */

        /** @noinspection PhpUndefinedFieldInspection */
        $sso = array(
            'username' => $user->username,
            'secret' => sha1($user->username . $sso_challenge . $key),
            'master_domain' => $host,
            'master_auth_uri' => $master_auth_uri,
            'lifetime' => time() + 3600,
            'target' => $target_host,
        );
        $cookie = base64_encode(serialize($sso));

        // Redirect to Target Server

        $url = $host_with_protocol . $master_auth_uri;
        $params = 'sso_referer=' . urlencode($url) . '&sso_cookie=' . urlencode($cookie);
        $redirect = $target_host;

        $last = substr($redirect, -1, 1);
        if (strcmp($last, '/') !== 0) {
            $redirect .= '/';
        }

        $redirect_url = $redirect . '?' . $params;
        return redirect($redirect_url);
    }
}
