<?php

namespace App\Http\Requests\Payments;

use Illuminate\Foundation\Http\FormRequest;

class UploadDocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document' => 'required|mimes:pdf|max:40000'
        ];
    }

    public function messages(){
        return [
            'document.required' => 'Παρακαλώ επιλέξτε αρχείο',
            'document.max' => 'Το αρχείο πρέπει να είναι έως 4mb',
            'document.mimes' => 'Το αρχείο πρέπει να είναι της μορφής pdf',
        ];
    }
}
