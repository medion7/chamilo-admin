<?php

if (!function_exists('is_menu_item_active')) {
    function is_menu_item_active($modelSlug)
    {
        return request()->is(config("admin.prefix")."/".$modelSlug.'*');
    }
}







