<?php


namespace App\Listeners;


use App\Mail\Admins\NewApplication;
use App\Mail\Admins\NewPaymentDocument;
use App\Mail\Students\ApplicationApproved;
use App\Models\User;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class ApplicationEventSubscriber
{
    /**
     * Handle user login events.
     */
    public function handleApplicationCreated($event) {
        $admins = User::ofRole('admin')->get();
        Mail::to($admins)->send(new NewApplication($event->application));
    }

    /**
     * @param $event
     */
    public function handleApplicationApproved($event){
        $payment = $event->application->payments()->orderBy('payment_date','asc')->first();

        if($payment){
            $payment->update(['notified'=>true]);
        }

        $message = new ApplicationApproved($event->application,$payment);
//        if($event->application->requires_invoice){
//            $message->attach(Storage::url('invoice_template.pdf'));
//        }
        Mail::to($event->application->user)->send($message);
    }

    /**
     * @param $event
     */
    public function handlePaymentDocumentUploaded($event){
        $admins = User::ofRole('admin')->get();
        Mail::to($admins)->send(new NewPaymentDocument($event->payment));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\ApplicationCreated',
            [ApplicationEventSubscriber::class, 'handleApplicationCreated']
        );

        $events->listen(
            'App\Events\ApplicationApproved',
            [ApplicationEventSubscriber::class, 'handleApplicationApproved']
        );

        $events->listen(
            'App\Events\PaymentDocumentUploaded',
            [ApplicationEventSubscriber::class, 'handlePaymentDocumentUploaded']
        );
    }
}
