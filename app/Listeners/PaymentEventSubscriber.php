<?php


namespace App\Listeners;

use App\Mail\Admins\NewPaymentDocument;
use App\Mail\Students\PaymentDocumentApproved;
use App\Mail\Students\PaymentDocumentRejected;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

class PaymentEventSubscriber
{

    /**
     * @param $event
     */
    public function handlePaymentDocumentUploaded($event){
        $admins = User::ofRole('admin')->get();
        Mail::to($admins)->send(new NewPaymentDocument($event->payment));
    }

    /**
     * @param $event
     */
    public function handlePaymentDocumentApproved($event){
        $student = $event->payment->application->user;
        Mail::to($student)->send(new PaymentDocumentApproved($event->payment));
    }

    /**
     * @param $event
     */
    public function handlePaymentDocumentRejected($event){
        $student = $event->payment->application->user;
        Mail::to($student)->send(new PaymentDocumentRejected($event->payment));
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     * @return void
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\PaymentDocumentUploaded',
            [PaymentEventSubscriber::class, 'handlePaymentDocumentUploaded']
        );

        $events->listen(
            'App\Events\PaymentDocumentApproved',
            [PaymentEventSubscriber::class, 'handlePaymentDocumentApproved']
        );

        $events->listen(
            'App\Events\PaymentDocumentRejected',
            [PaymentEventSubscriber::class, 'handlePaymentDocumentRejected']
        );
    }
}
