<?php

namespace App\Mail\Students;

use App\Models\Application;
use App\Models\Payment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ApplicationApproved extends Mailable
{
    use Queueable, SerializesModels;

    public $application,$payment;

    /**
     * Create a new message instance.
     *
     * @param Application $application
     * @param Payment $payment
     */
    public function __construct(Application $application, Payment $payment)
    {
        $this->application = $application;
        $this->payment = $payment;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Application Approved')->view('mail.students.application-approved',[
            'application' => $this->application,
            'payment' => $this->payment
        ]);
    }
}
