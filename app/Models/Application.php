<?php

namespace App\Models;

use App\Mail\Students\CertificateCreated;
use Barryvdh\DomPDF\Facade as PDF;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;

class Application extends Model
{
    use HasFactory;

    protected $guarded=[];

    protected $casts = [
        'certified' => 'boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function course(){
        return $this->belongsTo(Course::class,'course_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function status(){
        return $this->belongsTo(ApplicationStatus::class,'status_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function certificate(){
        return $this->morphOne(Document::class, 'documentable');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeApproved($query){
        return $query->whereHas('status',function($statusQuery){
            $statusQuery->where('application_statuses.name','approved');
        });
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function payments(){
        return $this->hasMany(Payment::class,'application_id');
    }

    /**
     * Generates the certificate for the application
     */
    public function generateCertificate(){
        $directory = 'certificates/'.$this->user->id;
        Storage::makeDirectory($directory);
        $finalPath = $directory.'/'.$this->course->code.'-'.Carbon::today()->toDateString().'.pdf';
        $pdf = PDF::loadView('certificates.certificate', [
            'application' => $this,
            'student' => $this->user
        ])->output();
        Storage::put($finalPath,$pdf);
        $documentType = DocumentType::where('name','certificate')->first();
        $this->certificate()->create([
            'type_id' => $documentType->id,
            'path' => $finalPath
        ]);
        Mail::to($this->user)->send(new CertificateCreated($this));
    }
}
