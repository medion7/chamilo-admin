<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];

    protected $casts = [
        'course_start' => 'datetime',
        'course_end' => 'datetime',
        'registration_start' => 'datetime',
        'registration_end' => 'datetime',
        'free' => 'boolean'
    ];

    use HasFactory;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function paymentSchedules(){
        return $this->hasMany(PaymentSchedule::class);
    }

}
