<?php

namespace App\Models;

use App\Tools\SoapWrapper\SoapWrapper;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
use Laravel\Fortify\TwoFactorAuthenticatable;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens;
    use HasFactory;
    use HasProfilePhoto;
    use Notifiable;
    use TwoFactorAuthenticatable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'surname',
        'dad_name',
        'email',
        'password',
        'username',
        'specialty',
        'chamilo_id',
        'gender'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'two_factor_recovery_codes',
        'two_factor_secret',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'profile_photo_url',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_role_pivot');
    }

    /**
     * @param $query
     * @param $roleName
     * @return mixed
     */
    public function scopeOfRole($query,$roleName){
        return $query->whereHas('roles',function($roleQuery) use ($roleName){
            $roleQuery->where('name',$roleName);
        });
    }

    /**
     * @param $roleName
     * @return bool
     */
    public function hasRole($roleName)
    {
        return $this->roles()->where('name', $roleName)->exists();
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function degrees()
    {
        return $this->morphMany(Document::class, 'documentable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function applications()
    {
        return $this->hasMany(Application::class, 'user_id');
    }


    /**
     * @param $courseId
     * @return bool
     */
    public function hasAppliedForCourse($courseId){
        return $this->applications()->where('course_id',$courseId)
            ->whereHas('status',function($statusQuery){
                $statusQuery->whereIn('application_statuses.name',['pending','approved']);
            })->exists();
    }

    /**
     * @param $courseId
     * @return bool
     */
    public function hasApprovedApplicationForCourse($courseId){
        return $this->applications()->where('course_id',$courseId)->approved()->exists();
    }

    /**
     * @param $courseId
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Relations\HasMany|object|null
     */
    public function approvedApplicationForCourse($courseId){
        return $this->applications()->where('course_id',$courseId)->approved()->first();
    }

    /**
     * @param $courseId
     * @return bool
     */
    public function hasCertificate($courseId){
        return $this->applications()->where('course_id',$courseId)->approved()
            ->where('certified',true)
            ->whereHas('certificate')
            ->exists();
    }
}
