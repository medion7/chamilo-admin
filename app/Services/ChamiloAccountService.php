<?php


namespace App\Services;


use App\Models\User;
use App\Tools\SoapWrapper\SoapWrapper;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class ChamiloAccountService
{

    /**
     * @param $chamilo_role
     * @param User $user
     * @return false|mixed
     * @throws \App\Tools\SoapWrapper\Exceptions\ServiceAlreadyExists
     */
    public function create($chamilo_role,User $user){
        $namespace = 'Users';
        switch ($chamilo_role) {
            case "student":
                $chamilo_status = '5';
                break;
            case "teacher":
                $chamilo_status = '1';
                break;
        }
        $chamilo_user_parameters = array(
            'firstname' => $user->name,
            'lastname' => $user->name,
            'status' => $chamilo_status,
            'email' => $user->email,
            'loginname' => $user->username,
            'password' => Hash::make(Str::random(8)),
            'phone' => '',
            'language' => 'english',
            'expiration_date' => '',
            'original_user_id_name' => 'laravel_username',
            'original_user_id_value' => $user->username,
            'extra' => [],
            'active' => 1
        );
        $soapWrapper = new SoapWrapper($namespace, ['host'=>config('services.chamilo.host'),'key'=>config('services.chamilo.key')]);
        return $soapWrapper->call($namespace . '.WSCreateUser', $chamilo_user_parameters);
    }

}
