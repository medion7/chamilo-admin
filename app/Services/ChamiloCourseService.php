<?php


namespace App\Services;
use App\Models\Course;
use App\Models\User;
use App\Tools\SoapWrapper\SoapWrapper;
use Illuminate\Support\Facades\Log;

class ChamiloCourseService
{
    /**
     * @param Course $course
     * @param User $user
     * @param string $role
     * @return false|mixed
     */
    public function subscribe(Course $course,User $user,$role = 'student'){
        $namespace = 'Users';
        $roleId = $role == 'teacher' ? 1 : 5;
        $parameters = array(
            'course' => $course->code,
            'user_id' => $user->chamilo_id,
            'status' => $roleId
        );
        try {
            $soapWrapper = new SoapWrapper($namespace, ['host' => config('services.chamilo.host'), 'key' => config('services.chamilo.key')]);
            $response = $soapWrapper->call($namespace . '.WSSubscribeUserToCourseSimple', $parameters);
        } catch (\Exception $e) {
            Log::error("Soap Fault:" . $e->getMessage());
            $response = false;
        }
        return $response;
    }

    /**
     * @param $userId
     * @param $courseCode
     * @return \Illuminate\Http\JsonResponse
     */
    public function courseProgressTest($userId,$courseCode){
        $parameters = [
            'user_id' => $userId,
            'course_code' => $courseCode
        ];
        $namespace = 'Users';
        try {
            $soapWrapper = new SoapWrapper($namespace, ['host' => config('services.chamilo.host'), 'key' => config('services.chamilo.key')]);
            $response = $soapWrapper->call($namespace . '.getUserCourseProgress', $parameters);
            $response = json_decode($response);
        } catch (\Exception $e) {
            Log::error("Soap Fault:" . $e->getMessage());
            $response = false;
        }
        return $response;
    }
}
