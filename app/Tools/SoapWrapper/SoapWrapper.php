<?php

namespace App\Tools\SoapWrapper;

use Closure;
use SoapClient;
use App\Tools\SoapWrapper\Exceptions\ServiceNotFound;
use App\Tools\SoapWrapper\Exceptions\ServiceAlreadyExists;
use App\Tools\SoapWrapper\Exceptions\ServiceMethodNotExists;

class SoapWrapper
{
    /**
     * @var array
     */
    protected $services;

    /**
     * SoapWrapper constructor
     * @param $namespace
     * @param $chamilo_platform
     * @throws ServiceAlreadyExists
     */
    public function __construct($namespace,$chamiloConnectionDetails)
    {
        $host = $chamiloConnectionDetails['host'];
        $key = $chamiloConnectionDetails['key'];
        $this->services = [];
        $this->add($namespace, function ($service) use ($host,$key) {
            /** @noinspection PhpUndefinedMethodInspection */
            $service
                ->endpoint('/main/webservices/registration.soap.php?wsdl')
                ->host($host)
                ->key($key)
                ->trace(true)
                ->cache(WSDL_CACHE_NONE);
        });
    }

    /**
     * Add a new service to the wrapper
     *
     * @param string $name
     * @param Closure $closure
     *
     * @return $this
     * @throws ServiceAlreadyExists
     */
    public function add($name, Closure $closure)
    {
        if (!$this->has($name)) {

            $service = new Service();

            $closure($service);

            $this->services[$name] = $service;

            return $this;
        }

        throw new ServiceAlreadyExists("Service '" . $name . "' already exists.");
    }

    /**
     * Add services by array
     *
     * @param array $services
     *
     * @return $this
     *
     * @throws ServiceAlreadyExists
     * @throws ServiceMethodNotExists
     */
    public function addByArray(array $services = [])
    {
        if (!empty($services)) {
            foreach ($services as $name => $methods) {
                if (!$this->has($name)) {
                    $service = new Service();

                    foreach ($methods as $method => $value) {
                        if (method_exists($service, $method)) {
                            $service->{$method}($value);
                        } else {
                            throw new ServiceMethodNotExists(sprintf(
                                "Method '%s' does not exists on the %s service.",
                                $method,
                                $name
                            ));
                        }
                    }

                    $this->services[$name] = $service;

                    continue;
                }

                throw new ServiceAlreadyExists(sprintf(
                    "Service '%s' already exists.",
                    $name
                ));
            }
        }

        return $this;
    }

    /**
     * Get the client
     *
     * @param string $name
     * @param Closure $closure
     *
     * @return mixed
     * @throws ServiceNotFound
     */
    public function client($name, Closure $closure = null)
    {
        if ($this->has($name)) {
            /** @var Service $service */
            $service = $this->services[$name];

            if (is_null($service->getClient())) {
                $client = new Client($service->getWsdl(), $service->getOptions(), $service->getHeaders());

                $service->client($client);
            } else {
                $client = $service->getClient();
            }

            return $closure($client);
        }

        throw new ServiceNotFound("Service '" . $name . "' not found.");
    }

    /**
     * A easy access call method
     *
     * @param string $call
     * @param array $data
     *
     * @return mixed
     * @throws ServiceNotFound
     */
    public function call($call, $data = [], $options = [])
    {
        list($name, $function) = explode('.', $call, 2);

        $service = $this->services[$name];

        $data['secret_key'] = $this->get_key($service->getHost(),$service->getKey());
        $params[0] = $data;

        return $this->client($name, function ($client) use ($function, $params, $options) {
            /** @var Client $client */
            return $client->SoapCall($function, $params, $options);
        });
    }

    private function get_key($host,$key)
    {
        $chamiloServerUrl = $host;
        $chamilo_app_key = $key;

        $chamilo_ip = $this->get_chamilo_ip($chamiloServerUrl);

        $secret_key = sha1($chamilo_ip . $chamilo_app_key);

        return $secret_key;
    }

    private function get_chamilo_ip($chamiloServerUrl) {

        $chamiloServerUrl = rtrim($chamiloServerUrl, '/');
        $client_ip = null;

        // Get the client IP address (to be used for signing SOAP requests)
        $url = $chamiloServerUrl . '/main/webservices/testip.php';
        if (ini_get('allow_url_fopen')) {
            $client_ip= file_get_contents($url);
        } else {
            if (!($ch = curl_init())) {
                throw new Exception('Your hosting must support cURL or allow http wrapper for file_get_contents.');
            } else {
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                $client_ip = curl_exec($ch);
                curl_close($ch);
                unset($ch);
            }
        }

        return $client_ip;
    }

    /**
     * Check if wrapper has service
     *
     * @param string $name
     *
     * @return bool
     */
    public function has($name)
    {
        return (array_key_exists($name, $this->services));
    }
}
