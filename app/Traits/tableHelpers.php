<?php


namespace App\Traits;


trait tableHelpers
{

    /** Apply search on the users table and then on related tables
     * @param $searchableColumnsOnUsers
     * @param $relationsMapping
     * @param $customersQuery
     * @param $term
     */
    protected function applySearch($searchableColumnsOnUsers, $relationsMapping, $customersQuery, $term)
    {
        $customersQuery->where(function ($whereQuery) use ($searchableColumnsOnUsers, $term, $relationsMapping) {
            foreach ($searchableColumnsOnUsers as $searchableColumn) {
                $whereQuery->orWhere($searchableColumn, 'like', "%$term%");
            }
            $this->applyRelationsSearch($relationsMapping, $whereQuery, $term);
        });
    }

    /** Apply search on related tables
     * @param $relationsMapping
     * @param $customersQuery
     * @param $term
     */
    protected function applyRelationsSearch($relationsMapping, $customersQuery, $term)
    {
        $customersQuery->Orwhere(function ($whereQuery) use ($relationsMapping, $term) {
            foreach ($relationsMapping as $relationshipName => $relationshipColumns) {
                $whereQuery->orWhereHas($relationshipName, function ($relationshipQuery) use ($relationshipColumns, $term) {
                    $relationshipQuery->where(function ($relationshipSearchQuery) use ($relationshipColumns, $term) {
                        foreach ($relationshipColumns as $searchableRelationshipColumn) {
                            $relationshipSearchQuery->orWhere($searchableRelationshipColumn, 'like', "%$term%");
                        }
                    });
                });
            }
        });
    }

}
