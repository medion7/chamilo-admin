<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->text('body')->nullable();
            $table->text('certificate_description')->nullable();
            $table->float('price')->nullable();
            $table->text('bank_account')->nullable();
            $table->text('program_code')->nullable();
            $table->date('course_start');
            $table->date('course_end');
            $table->date('registration_start');
            $table->date('registration_end');
            $table->text('photo_path')->nullable();
            $table->string('code');
            $table->boolean('free')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
