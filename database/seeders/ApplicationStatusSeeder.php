<?php

namespace Database\Seeders;

use App\Models\ApplicationStatus;
use Illuminate\Database\Seeder;

class ApplicationStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ApplicationStatus::create([
           'name'=>'pending',
           'label'=>'Pending',
        ]);

        ApplicationStatus::create([
            'name'=>'approved',
            'label'=>'Approved',
        ]);

        ApplicationStatus::create([
            'name'=>'rejected',
            'label'=>'Rejected',
        ]);
    }
}
