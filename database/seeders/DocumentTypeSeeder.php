<?php

namespace Database\Seeders;

use App\Models\DocumentType;
use Illuminate\Database\Seeder;

class DocumentTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        {
            DocumentType::create([
                'name' => 'invoice',
                'label' => 'Invoice'
            ]);

            DocumentType::create([
                'name' => 'degree',
                'label' => 'Degree'
            ]);

            DocumentType::create([
                'name' => 'certificate',
                'label' => 'Certificate'
            ]);
        }
    }
}
