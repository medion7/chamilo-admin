<?php

namespace Database\Seeders;

use App\Models\PaymentStatus;
use Illuminate\Database\Seeder;

class PaymentStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentStatus::create([
           'name'=>'pending',
           'label'=>'Pending',
        ]);

        PaymentStatus::create([
            'name'=>'approved',
            'label'=>'Approved',
        ]);

        PaymentStatus::create([
            'name'=>'rejected',
            'label'=>'Rejected',
        ]);
    }
}
