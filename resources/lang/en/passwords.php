<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Ο κωδικός σας άλλαξε επιτυχώς!',
    'sent' => 'Σας έχουμε στείλει email για την αλλαγή του κωδικού σας.',
    'throttled' => 'Παρακαλώ περιμέμενε πριν ξαναδοκιμάσετε.',
    'token' => 'This password reset token is invalid.',
    'user' => "Δεν μπορούμε να βρούμε αυτόν τον χρήστη.",

];
