@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/vendors/@coreui/chartjs/css/coreui-chartjs.css')}}" rel="stylesheet">
@endsection
@section('content')

    <div class="card">
        <div class="card-header">ADD APPLICATION</div>
        <form class="form-horizontal" action="{{action('\App\Http\Controllers\Admin\ApplicationsController@store')}}" method="post">
            <div class="card-body">

                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

    {{csrf_field()}}
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" >Id</label>
                        <div class="col-md-9">
                            <input class="form-control" id="" type="text" name="Id" placeholder="Enter Id" autocomplete="" value="{{old('Id')}}"><span class="help-block">Please enter Id</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" >User Id</label>
                        <div class="col-md-9">
                            <input class="form-control" id="" type="text" name="User_id" placeholder="Enter User Id" autocomplete="" value="{{old('User_Id')}}"><span class="help-block">Please enter User Id</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" >User Id</label>
                        <div class="col-md-9">
                            <input class="form-control" id="" type="text" name="User_id" placeholder="Enter User Id" autocomplete="" value="{{old('User_Id')}}"><span class="help-block">Please enter User Id</span>
                        </div>
                    </div>

                    <div class="card-footer">
                        <button type="submit"  class="btn btn-primary">ADD APPLICATION</button>
                        <a href="{{action('\App\Http\Controllers\Admin\ApplicationsController@index')}}" class="btn btn-sm btn-danger" type="">BACK</a>
                    </div>
                    </form>
            </div>
@endsection
