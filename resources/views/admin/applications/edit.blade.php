@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/vendors/@coreui/chartjs/css/coreui-chartjs.css')}}" rel="stylesheet">
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <br />
        <h3>Edit Course</h3>
        <br />
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('\App\Http\Controllers\Admin\ApplicationsController@update', $id)}}">
            @csrf
            @method('patch')
            <label class="col-md-3 col-form-label" >Id</label>
            <div class="form-group">
                <input type="integer" name="id" class="form-control" value="{{old('Id',$application->id)}}" placeholder=""/>
            </div>

            <label class="col-md-3 col-form-label" >User Id</label>
            <div class="form-group">
                <input type="integer" name="user_id" class="form-control" value="{{old('user_id',$application->user_id)}}" placeholder=""/>
            </div>

            <label class="col-md-3 col-form-label" >Status Id</label>
            <div class="form-group">
                <input type="integer" name="status_id" class="form-control" value="{{old('status_id',$application->status_id)}}" placeholder=""/>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="Edit" />
            </div>
        </form>
    </div>
</div>
@endsection
