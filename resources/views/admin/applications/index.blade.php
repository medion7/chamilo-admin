@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
    @endif
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
        <div class="col-md-3">
            <label for="status_filter">Επιλογή Κατάστασης</label>
            <select id="status_filter" class="form-control">
                <option value="" {{!Request::has('status') ? 'selected' : ''}}>Όλες</option>
                @foreach($applicationStatuses as $applicationStatus)
                    <option
                        value="{{$applicationStatus->name}}" {{Request::input('status') === $applicationStatus->name ? 'selected' : ''}}>
                        {{$applicationStatus->label}}
                    </option>
                @endforeach
            </select>
        </div>
        <div class="col-md-3">
            <div style="display: inline-block">
                <label for="search-input">Αναζήτηση</label>
                <input type="text" id="search-input" class="form-control" placeholder="Αναζήτηση" value="{{Request::input('term')}}"/>
            </div>
            <div style="display: inline-block">
                <button class="btn-sm btn-primary" style="display:inline-block;" id="search-button">
                    <i class="cil-search"></i>
                </button>
            </div>
        </div>
    </div>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">ΧΡΗΣΤΗΣ</th>
            <th scope="col">ΚΑΤΑΣΤΑΣΗ</th>
            <th scope="col">ΜΑΘΗΜΑ</th>
            <th scope="col">ΠΛΗΡΩΜΕΣ</th>
            <th scope="col">ΤΙΜΟΛΟΓΙΟ</th>
            <th scope="col">ΕΝΕΡΓΕΙΕΣ</th>
        </tr>
        </thead>
        <tbody>
        @foreach($applications as $application)
            <tr>
                <th scope="row">{{$application->id}}</th>
                <td>{{$application->user->name}}</td>
                <td>{{$application->status->label}}</td>
                <td>{{$application->course->title}}</td>
                <td>
                    @if($application->status->name === 'approved')
                        <a href="{{route('admin.applications.index-payments',['application' => $application->id])}}">
                            <svg class="c-icon">
                                <use xlink:href=" {{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-euro')}}"/>
                            </svg>
                        </a>
                    @endif
                </td>
                <td>
                    {{$application->requires_invoice ? 'NAI' : 'ΌXI'}}
                </td>
                <td>
                    @if($application->status->name === 'pending')
                        <div onclick="acceptApplication({{$application->id}})" class="action-buttons">
                            <button class="btn btn-sm btn-success">ACCEPT</button>
                        </div>
                        <div onclick="rejectApplication({{$application->id}})" class="action-buttons">
                            <button class="btn btn-sm btn-danger">REJECT</button>
                        </div>
                    @endif
                </td>
        @endforeach
    </table>
    {!! $applications->appends(request()->except('page'))->links() !!}
@endsection
@section('bottomAssets')
    <!-- Plugins and scripts required by this view-->
    <script type="text/javascript" src="{{asset('admin_ui/vendors/@coreui/utils/js/coreui-utils.js')}}"></script>
    <script type="text/javascript">
        $("#status_filter").on('change', function () {
            let statusFilterValue = $("#status_filter").val();
            var url = new URL(window.location.href);
            var search_params = url.searchParams;
            search_params.set('status', statusFilterValue);
            url.search = search_params.toString();
            window.location.href = url.toString();
        });

        $("#search-button").on('click', function () {
            let searchInputValue = $("#search-input").val();
            var url = new URL(window.location.href);
            var search_params = url.searchParams;
            search_params.set('term', searchInputValue);
            url.search = search_params.toString();
            window.location.href = url.toString();
        });

        function acceptApplication(applicationId) {
            let r = window.confirm('Η αίτηση θα γίνει αποδεκτή')
            if (r) {
                $.ajax({
                    url: '/{{config('app.admin_prefix')}}/applications/' + applicationId + '/accept',
                    type: 'PATCH',
                    success: function (result) {
                        alert(result.message);
                        window.location.reload();
                    }
                });
            }
        }

        function rejectApplication(applicationId) {
            let r = window.confirm('Η αίτηση θα απορριφθεί')
            if (r) {
                $.ajax({
                    url: '/{{config('app.admin_prefix')}}/applications/' + applicationId + '/reject',
                    type: 'PATCH',
                    success: function (result) {
                        alert(result.message);
                        window.location.reload();
                    }
                });
            }
        }
    </script>
@endsection

