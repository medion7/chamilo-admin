@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/css/font-awesome.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="card">
        <div class="card-header">ΠΡΟΣΘΗΚΗ ΜΑΘΗΜΑΤΟΣ</div>
        <form class="form-horizontal" action="{{action('\App\Http\Controllers\Admin\CoursesController@store')}}"
              method="post">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{csrf_field()}}
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Τίτλος</label>
                    <div class="col-md-9">
                        <input class="form-control" id="title" type="text" name="title" placeholder="Εισάγετε Τίτλο"
                               autocomplete="" value="{{old('title')}}"><span class="help-block">Παρακαλώ εισάγετε τίτλο μαθήματος</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Κωδικός μαθήματος</label>
                    <div class="col-md-9">
                        <input class="form-control" id="code" type="text" name="code"
                               placeholder="Εισάγετε κωδικό μαθήματος"
                               autocomplete="" value="{{old('code')}}"><span class="help-block">Παρακαλώ εισάγετε κωδικό μαθήματος</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Κωδικός Προγράμματος</label>
                    <div class="col-md-9">
                        <input class="form-control" id="program_code" type="text" name="program_code"
                               placeholder="Εισάγετε κωδικό προγράμματος"
                               autocomplete="" value="{{old('program_code')}}">
                        <span class="help-block">Παρακαλώ εισάγετε κωδικό προγράμματος</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Περιγραφή</label>
                    <div class="col-md-9">
                        <textarea name="body" id="body">{{old('body')}}</textarea>
                    </div>
                </div>


                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="certificate_description">Περιγραφή
                        Πιστοποιητικού</label>
                    <div class="col-md-9">
                            <textarea class="form-control" id="certificate_description" type="text"
                                      name="certificate_description"
                                      placeholder="">{{old('certificate_description')}}</textarea>
                    </div>
                </div>

                @include('admin.fields.checkbox.view',[
               'fieldName'=>'free',
               'label'=>'Δωρεάν',
               "defaultValue"=>old('free'),
               ])

                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Τιμή</label>
                    <div class="col-md-9">
                        <input class="form-control" id="" type="text" name="price" placeholder="Εισάγετε Τιμή"
                               autocomplete="" value="{{old('price')}}"><span class="help-block">Παρακαλώ εισάγετε τιμή μαθήματος</span>
                    </div>
                </div>
                <div class="form-group row">
                    @include('admin.fields.datepicker.view',[
                       'fieldName'=>'registration_start',
                       'label'=>'Έναρξη Εγγραφών',
                       "defaultValue"=>old('registration_start'),
                       ])
                </div>
                <div class="form-group row">
                    @include('admin.fields.datepicker.view',[
                       'fieldName'=>'registration_end',
                       'label'=>'Λήξη Εγγραφών',
                       "defaultValue"=>old('registration_end'),
                       ])
                </div>
                <div class="form-group row">
                    @include('admin.fields.datepicker.view',[
                       'fieldName'=>'course_start',
                       'label'=>'Έναρξη Μαθημάτων',
                       "defaultValue"=>old('course_start'),
                       ])
                </div>
                <div class="form-group row">
                    @include('admin.fields.datepicker.view',[
                       'fieldName'=>'course_end',
                       'label'=>'Λήξη Μαθημάτων',
                       "defaultValue"=>old('course_end'),
                       ])
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">ΠΡΟΣΘΗΚΗ ΜΑΘΗΜΑΤΟΣ</button>
                    <a href="{{action('\App\Http\Controllers\Admin\CoursesController@index')}}"
                       class="btn btn-danger" type="">ΠΙΣΩ</a>
                </div>
            </div>
        </form>
        <script src="https://cdn.ckeditor.com/ckeditor5/23.1.0/classic/ckeditor.js"></script>
        <script>
            ClassicEditor
                .create(document.querySelector('#body'))
                .catch(error => {
                    console.error(error);
                });

            ClassicEditor
                .create(document.querySelector('#certificate_description'))
                .catch(error => {
                    console.error(error);
                });
        </script>
        @endsection
        @section('bottomAssets')
            <script type="text/javascript" src="{{asset('admin_ui/bower_components/moment/moment.js')}}"></script>
            <script type="text/javascript"
                    src="{{asset('admin_ui/bower_components/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js')}}"></script>
            <link rel="stylesheet"
                  href="{{asset('admin_ui/bower_components/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css')}}"/>
    @include('admin.fields.datepicker.scripts',['fieldName'=>'registration_start','defaultValue'=>old('registration_start')])
    @include('admin.fields.datepicker.scripts',['fieldName'=>'registration_end','defaultValue'=>old('registration_end')])
    @include('admin.fields.datepicker.scripts',['fieldName'=>'course_start','defaultValue'=>old('course_start')])
    @include('admin.fields.datepicker.scripts',['fieldName'=>'course_end','defaultValue'=>old('course_end')])
    @include('admin.fields.checkbox.scripts')
@endsection
