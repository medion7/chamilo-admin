@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/css/font-awesome.min.css')}}" rel="stylesheet">

@endsection
@section('content')
    <div class="card">
        <div class="card-header">
            <div class="card-header">ΕΠΕΞΕΡΓΑΣΙΑ ΜΑΘΗΜΑΤΩΝ</div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form class="form-horizontal" method="post" enctype="multipart/form-data"
                  id="update-form"
                  action="{{action('\App\Http\Controllers\Admin\CoursesController@update', $course->id)}}">
                @csrf
                @method('patch')
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="title_field">Εικόνα</label>
                        <div class="col-md-9">
                            <div class="nest" id="DropZoneClose"  @if(!empty($course->photo_path)) style="display: none;" @endif>
                                <div class="body-nest" id="DropZone">
                                    <div id="myDropZone">
                                    </div>
                                </div>
                            </div>
                            @if(!empty($course->photo_path))
                                <img src="{{Storage::disk('public')->url($course->photo_path)}}" class="img-preview">
                                <span class="remove-img">Remove file</span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="title_field">Τίτλος</label>
                        <div class="col-md-9">
                            <input class="form-control" id="title_field" type="text" name="title" placeholder="" value="{{old('title',$course->title)}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Κωδικός μαθήματος</label>
                        <div class="col-md-9">
                            <input class="form-control" id="code" type="text" name="code"
                                   placeholder="Εισάγετε κωδικό μαθήματος"
                                   autocomplete="" value="{{old('code',$course->code)}}">
                            <span class="help-block">Παρακαλώ εισάγετε κωδικό μαθήματος</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Κωδικός Προγράμματος</label>
                        <div class="col-md-9">
                            <input class="form-control" id="program_code" type="text" name="program_code"
                                   placeholder="Εισάγετε κωδικό προγράμματος"
                                   autocomplete="" value="{{old('program_code',$course->program_code)}}">
                            <span class="help-block">Παρακαλώ εισάγετε κωδικό προγράμματος</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="description_field">Περιγραφή</label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="description_field" type="text" name="body"
                                      placeholder="">{{old('body',$course->body)}}</textarea>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="certificate_description">Περιγραφή Πιστοποιητικού</label>
                        <div class="col-md-9">
                            <textarea class="form-control" id="certificate_description" type="text" name="certificate_description"
                                      placeholder="">{{old('certificate_description',$course->certificate_description)}}</textarea>
                        </div>
                    </div>

                    @include('admin.fields.checkbox.view',[
                     'fieldName'=>'free',
                     'label'=>'Δωρεάν',
                     "defaultValue"=>old('free',$course->free),
                     ])

                    <div class="form-group row">
                        <label class="col-md-3 col-form-label" for="price_field">Τιμή</label>
                        <div class="col-md-9">
                            <input class="form-control" id="price_field" type="text" name="price" placeholder=""
                                   value="{{old('price_field',$course->price)}}">
                        </div>
                    </div>

                    <div class="form-group row">
                        @include('admin.fields.datepicker.view',[
                            'fieldName'=>'registration_start',
                            'label'=>'Έναρξη Εγγραφών',
                            "defaultValue"=>old('registration_start',$course->registration_start->format('d/m/Y')),
                            ])
                    </div>

                    <div class="form-group row">
                        @include('admin.fields.datepicker.view',[
                            'fieldName'=>'registration_end',
                            'label'=>'Λήξη Εγγραφών',
                            "defaultValue"=>old('registration_end',$course->registration_end->format('d/m/Y')),
                            ])
                    </div>

                    <div class="form-group row">
                        @include('admin.fields.datepicker.view',[
                            'fieldName'=>'course_start',
                            'label'=>'Έναρξη Μαθημάτων',
                            "defaultValue"=>old('course_start',$course->course_start->format('d/m/Y')),
                            ])
                    </div>

                    <div class="form-group row">
                        @include('admin.fields.datepicker.view',[
                            'fieldName'=>'course_end',
                            'label'=>'Λήξη Μαθημάτων',
                            "defaultValue"=>old('course_end',$course->course_end->format('d/m/Y')),
                            ])
                    </div>

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">ΑΠΟΘΗΚΕΥΣΗ</button>
                        <a href="{{action('\App\Http\Controllers\Admin\CoursesController@index')}}"
                           class="btn btn-danger" type="">ΠΙΣΩ</a>
                    </div>

                </div>
            </form>
        </div>
    </div>
@endsection
@section('bottomAssets')
    <script src="{{asset('js/dropzone/dropzone.js')}}"></script>
    <link href="{{asset('js/dropzone/dropzone.css')}}" rel="stylesheet">
    <style>
        div#myDropZone {
            width: 100px;
            min-height: 100px;
            background-color: gray;
            margin-bottom:20px;
            cursor:pointer;
            border-radius:10px;
        }

        .img-preview{
            border-radius:10px;
            max-height: 100px;
        }
        .dz-image img{
            border-radius:10px;
        }
        .dz-details{
            background-color:white;
            cursor:default;
            position:absolute;
            width:100%;
            padding-right:20px;
        }
        .dz-size{
            display:inline-block!important;
        }
        .dz-filename{
            display:inline-block!important;
        }
        .dz-remove{
            position: absolute;
            top: 0;
            right: 30px;
        }

        .remove-img{
            position: absolute;
            top: 0;
            right: 30px;
            cursor:pointer;
        }
        .dz-success-mark{
            display: none;
        }
        .dz-error-mark{
            display:none;
        }
    </style>
    <script src="https://cdn.ckeditor.com/ckeditor5/23.1.0/classic/ckeditor.js"></script>
    <script>
        ClassicEditor
            .create(document.querySelector('#description_field'))
            .catch(error => {
                console.error(error);
            });

        ClassicEditor
            .create(document.querySelector('#certificate_description'))
            .catch(error => {
                console.error(error);
            });

        $(function () {
            $("div#myDropZone").dropzone({
                url: "{{action('\App\Http\Controllers\Admin\CoursesController@updateImage', $course->id)}}",
                addRemoveLinks:true,
                init: function() {
                    this.on("sending",  function(file, xhr, data) {
                        data.append("_token", '{{csrf_token()}}');
                    });
                    this.on("removedfile",  function(file, xhr, data) {
                        deleteImage();
                    });
                }
            });
            $(".remove-img").on('click',function(){
                deleteImage();
            });
            function deleteImage(){
                $.post("{{action('\App\Http\Controllers\Admin\CoursesController@deleteImage', $course->id)}}").then(function(){
                    window.location.reload();
                })
            }
        });

    </script>
    <script type="text/javascript" src="{{asset('admin_ui/bower_components/moment/moment.js')}}"></script>
    <script type="text/javascript"
            src="{{asset('admin_ui/bower_components/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js')}}"></script>
    <link rel="stylesheet"
          href="{{asset('admin_ui/bower_components/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css')}}"/>
    @include('admin.fields.datepicker.scripts',['fieldName'=>'registration_start','defaultValue'=>old('registration_start',$course->registration_start->format('d/m/Y'))])
    @include('admin.fields.datepicker.scripts',['fieldName'=>'registration_end','defaultValue'=>old('registration_end',$course->registration_end->format('d/m/Y'))])
    @include('admin.fields.datepicker.scripts',['fieldName'=>'course_start','defaultValue'=>old('course_start',$course->course_start->format('d/m/Y'))])
    @include('admin.fields.datepicker.scripts',['fieldName'=>'course_end','defaultValue'=>old('course_end',$course->course_end->format('d/m/Y'))])
    @include('admin.fields.checkbox.scripts')

@endsection
