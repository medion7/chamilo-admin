@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/css/font-awesome.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
    @endif
    <body onload="sortByDate()">

    <div class="row" style="margin-top:20px; margin-bottom:20px;">
        <div class="col-md-12">
            <a href="{{action('\App\Http\Controllers\Admin\CoursesController@create')}}" class="btn btn-primary mb-2">+ ΠΡΟΣΘΗΚΗ
                ΜΑΘΗΜΑΤΟΣ</a>
        </div>
        <div class="col-md-3">
            <div style="display: inline-block">
                <label for="search-input">Αναζήτηση</label>
                <input type="text" id="search-input" class="form-control" placeholder="Αναζήτηση"
                       value="{{Request::input('term')}}"/>
            </div>
            <div style="display: inline-block">
                <button class="btn-sm btn-primary" style="display:inline-block;" id="search-button">
                    <i class="cil-search"></i>
                </button>
            </div>
        </div>
    </div>
    <table class="table" id="CoursesTable">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">ΤΙΤΛΟΣ</th>
            <th scope="col">ΚΩΔΙΚΟΣ ΠΡΟΓΡΑΜΜΑΤΟΣ</th>
            <th scope="col">ΤΙΜΗ</th>
            <th scope="col">ΔΗΜΙΟΥΡΓΙΑ</th>
            <th scope="col">ΠΕΡΙΟΔΟΣ ΕΓΓΡΑΦΩΝ</th>
            <th scope="col">ΠΕΡΙΟΔΟΣ ΜΑΘΗΜΑΤΩΝ</th>
            <th scope="col">ΔΟΣΕΙΣ</th>
            <th scope="col">ΕΝΕΡΓΕΙΕΣ</th>
        </tr>
        </thead>
        <tbody>
        @foreach($courses as $course)
            <tr>
                <th scope="row">{{$course->id}}</th>
                <td>{{$course->title}}</td>
                <td>{{$course->program_code}}</td>
                <td>{{$course->price}}</td>
                <td>{{$course->created_at->format('d/m/Y')}}</td>
                <td>{{Carbon\Carbon::parse($course->registration_start)->format('d/m/Y')}}
                    ~ {{Carbon\Carbon::parse($course->registration_end)->format('d/m/Y')}}</td>
                <td>{{Carbon\Carbon::parse($course->course_start)->format('d/m/Y')}}
                    ~ {{Carbon\Carbon::parse($course->course_end)->format('d/m/Y')}}</td>
                <td>
                    <a href="{{action('\App\Http\Controllers\Admin\PaymentSchedulesController@index', $course->id)}}">
                        <svg class="c-icon">
                            <use xlink:href=" {{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-euro')}}"/>
                        </svg>
                    </a>
                </td>
                <td>
                    <a href="{{action('\App\Http\Controllers\Admin\CoursesController@students',$course->id)}}">
                        <svg class="c-icon">
                            <use xlink:href="{{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-user')}}"></use>
                        </svg>
                    </a>
                    <a href="{{action('\App\Http\Controllers\Admin\CoursesController@edit',$course->id)}}">
                        <svg class="c-icon">
                            <use
                                xlink:href=" {{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-color-border')}}"/>
                        </svg>
                    </a>
                    <div onclick="deleteCourse({{$course->id}})" class="action-buttons">
                        <svg class="c-icon">
                            <use xlink:href="{{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-x-circle')}}"/>
                        </svg>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $courses->appends(request()->except('page'))->links() !!}
    </body>
@endsection
@section('bottomAssets')
    <!-- Plugins and scripts required by this view-->
    <script src="{{asset('admin_ui/vendors/@coreui/utils/js/coreui-utils.js')}}"></script>
    <script type="text/javascript">

        $("#search-button").on('click', function () {
            let searchInputValue = $("#search-input").val();
            var url = new URL(window.location.href);
            var search_params = url.searchParams;
            search_params.set('term', searchInputValue);
            url.search = search_params.toString();
            window.location.href = url.toString();
        });

        function deleteCourse(courseId) {
            let r = window.confirm('Το μάθημα πρόκειται να ΔΙΑΓΡΑΦΕΙ! ΕΙΣΤΕ ΣΙΓΟΥΡΟΣ ??')
            if (r) {
                $.ajax({
                    url: '{{config('app.admin_prefix')}}/courses/' + courseId,
                    type: 'DELETE',
                    success: function (result) {
                        alert(result.message);
                        window.location.reload();
                    }
                });
            }
        }

        function convertDate(d) {
            var p = d.split("/");
            return +(p[2] + p[1] + p[0]);
        }

        function sortByDate() {
            var tbody = document.querySelector("#CoursesTable tbody");
            var rows = [].slice.call(tbody.querySelectorAll("tr"));
            rows.sort(function (a, b) {
                return convertDate(a.cells[3].innerHTML) - convertDate(b.cells[3].innerHTML);
            });

            rows.forEach(function (v) {
                tbody.appendChild(v);
            });
        }
    </script>
@endsection

