<script type="text/javascript">
    $(function () {
        $(".form-check-input").on("change",function(){
            let modelSlug = $(this).attr("id").split("-")[0];
            let realInputElement = $("#"+modelSlug+"-value");
            realInputElement.val($(this).is(":checked") ? 1 : 0);
        })
    });
</script>