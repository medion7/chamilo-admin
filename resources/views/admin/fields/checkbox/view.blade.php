<div class="form-group row">
    <label class="col-md-3 col-form-label" for="{{$fieldName}}-input">{{$label}}</label>
    <div class="col-md-9">
        <div class="form-check checkbox">
            <input type="hidden" name="{{$fieldName}}" id="{{$fieldName}}-value" value="{{old($fieldName,isset($defaultValue) ? $defaultValue : 0)}}">
            <input class="form-check-input" id="{{$fieldName}}-input" type="checkbox" @if(old($fieldName,isset($defaultValue) ? $defaultValue : 0)) checked @endif @if(isset($disabled) && $disabled) disabled @endif>
            <label class="form-check-label" for="{{$fieldName}}-input">{{$label}}</label>
        </div>
        @if($errors->has($fieldName))
            <span class="invalid-feedback" role="alert">
              <strong>{{ $errors->first($fieldName) }}</strong>
            </span>
        @endif
    </div>
</div>
