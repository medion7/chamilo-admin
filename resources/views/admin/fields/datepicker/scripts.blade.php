<script>
    $(document).ready(function () {
        $('#{{$fieldName}}-input').datetimepicker(
            {
                format: 'DD/MM/YYYY',
                {{!empty($defaultValue) ? "defaultDate: moment(`$defaultValue`,`DD/MM/YYYY`)" : ''}}
            }
        );
    })
</script>
