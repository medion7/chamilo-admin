<label class="col-md-3 col-form-label" for="{{$fieldName}}-input">{{$label}}</label>
<div class="col-md-9">
    <div class="input-group date" id="{{$fieldName}}-input" data-target-input="nearest">
        <input type="text"  class="form-control @if($errors->has($fieldName)) is-invalid @endif" name="{{$fieldName}}"
               @if(isset($disabled) && $disabled ) disabled @endif  data-target="#{{$fieldName}}-input"/>
        <div class="input-group-append" data-target="#{{$fieldName}}-input" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
    </div>
</div>
