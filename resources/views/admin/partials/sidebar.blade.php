<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    <div class="c-sidebar-brand d-lg-down-none">
        <h3 class="c-sidebar-brand-full">Chamilo Admin</h3>
        <h3 class="c-sidebar-brand-minimized">CA</h3>
    </div>
    <ul class="c-sidebar-nav">
        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('admin.dashboard')}}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-speedometer')}}"></use>
                </svg> Πίνακας Ελέγχου</a>
        </li>

        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('admin.users.index')}}">
                <svg class="c-sidebar-nav-icon">
                <use xlink:href="{{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-user')}}"></use>
                </svg> Χρήστες </a>
        </li>

        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('admin.courses.index')}}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-book')}}"></use>
                </svg> Μαθήματα </a>
        </li>

        <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="{{route('admin.applications.index',['status'=>'pending'])}}">
                <svg class="c-sidebar-nav-icon">
                    <use xlink:href="{{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-description')}}"></use>
                </svg> Αιτήσεις </a>
        </li>
    </ul>



    <button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent" data-class="c-sidebar-minimized"></button>
</div>
