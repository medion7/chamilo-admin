@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/css/font-awesome.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="card">
        <div class="card-header">ΠΡΟΣΘΗΚΗ ΠΛΗΡΩΜΗΣ</div>
        <form class="form-horizontal" action="{{action('\App\Http\Controllers\Admin\PaymentSchedulesController@store',$course->id)}}"
              method="post">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{csrf_field()}}
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Ποσό Δόσης</label>
                    <div class="col-md-9">
                        <input class="form-control" id="" type="text" name="amount" placeholder="Εισάγετε Τιμή"
                               autocomplete="" value="{{old('amount')}}"><span class="help-block"> </span>
                    </div>
                </div>
                <div class="form-group row">
                    @include('admin.fields.datepicker.view',[
                       'fieldName'=>'payment_date',
                       'label'=>'Προθεσμία πληρωμής',
                       "defaultValue"=>old('payment_date'),
                       ])
                </div>
                <div class="card-footer">
                    <button type="submit" class="btn btn-primary">ΠΡΟΣΘΗΚΗ</button>
                    <a href="{{action('\App\Http\Controllers\Admin\PaymentSchedulesController@index',$course->id)}}"
                       class="btn btn-danger" type="">ΠΙΣΩ</a>
                </div>
            </div>
        </form>
        <script src="https://cdn.ckeditor.com/ckeditor5/23.1.0/classic/ckeditor.js"></script>
        <script>
            ClassicEditor
                .create(document.querySelector('#body'))
                .catch(error => {
                    console.error(error);
                });
        </script>
@endsection
@section('bottomAssets')
            <script type="text/javascript" src="{{asset('admin_ui/bower_components/moment/moment.js')}}"></script>
            <script type="text/javascript"
                    src="{{asset('admin_ui/bower_components/tempusdominus-bootstrap-4/build/js/tempusdominus-bootstrap-4.min.js')}}"></script>
            <link rel="stylesheet"
                  href="{{asset('admin_ui/bower_components/tempusdominus-bootstrap-4/build/css/tempusdominus-bootstrap-4.min.css')}}"/>
            @include('admin.fields.datepicker.scripts',['fieldName'=>'payment_date','defaultValue'=>old('payment_date')])
@endsection
