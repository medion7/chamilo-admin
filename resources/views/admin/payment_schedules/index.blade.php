@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/css/font-awesome.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
    @endif
    <body onload="sortByDate()">
        <div class="mb-4">
            <h3>Τίτλος μαθήματος: {{$course->title}}</h3>
        </div>
        <a href="{{action('\App\Http\Controllers\Admin\PaymentSchedulesController@create',$course->id)}}" class="btn btn-primary mb-2">+ ΠΡΟΣΘΗΚΗ ΠΛΗΡΩΜΗΣ</a>
        <table class="table" id="PaymentSchedulesTable">
            <thead class="thead-dark">
            <tr>
                <th scope="col">ΤΙΜΗ</th>
                <th scope="col">ΗΜΕΡΟΜΗΝΙΑ ΠΛΗΡΩΜΗΣ</th>
                <th scope="col">ΕΝΕΡΓΕΙΕΣ</th>
            </tr>
            </thead>
            <tbody>
            @foreach($course->paymentSchedules as $paymentSchedule)
                <tr>
                    <td>{{$paymentSchedule->amount}}</td>
                    <td class="sort-list">{{$paymentSchedule->payment_date->format('d/m/Y')}}</td>
                    <td>
                        <a href="{{action('\App\Http\Controllers\Admin\PaymentSchedulesController@edit',['course'=>$course->id,'payment_schedule'=>$paymentSchedule->id])}}">
                            <svg class="c-icon">
                                <use xlink:href=" {{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-color-border')}}"/>
                            </svg>
                        </a>
                        <div onclick="deletePaymentSchedule({{$course->id}},{{$paymentSchedule->id}})" class="action-buttons">
                            <svg class="c-icon">
                                <use xlink:href="{{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-x-circle')}}"/>
                            </svg>
                        </div>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </body>
@endsection
@section('bottomAssets')
    <!-- Plugins and scripts required by this view-->
    <script src="{{asset('admin_ui/vendors/@coreui/chartjs/js/coreui-chartjs.bundle.js')}}"></script>
    <script src="{{asset('admin_ui/vendors/@coreui/utils/js/coreui-utils.js')}}"></script>
    <script src="{{asset('admin_ui/js/main.js')}}"></script>
    <script>
        function deletePaymentSchedule(courseId, paymentScheduleId){
            let r = window.confirm('Η Δόση Πληρωμής πρόκειται να ΔΙΑΓΡΑΦΕΙ! ΕΙΣΤΕ ΣΙΓΟΥΡΟΣ ??')
            if(r){
                $.ajax({
                    url: '/{{config('app.admin_prefix')}}/courses/'+courseId+'/payment_schedules/'+paymentScheduleId,
                    type: 'DELETE',
                    success: function(result) {
                        alert(result.message);
                        window.location.reload();
                    }
                });
            }
        }
    </script>
    <script>
        function convertDate(d) {
            var p = d.split("/");
            return +(p[2]+p[1]+p[0]);
        }

        function sortByDate() {
            var tbody = document.querySelector("#PaymentSchedulesTable tbody");
            var rows = [].slice.call(tbody.querySelectorAll("tr"));
            rows.sort(function(a,b) {
                return convertDate(a.cells[1].innerHTML) - convertDate(b.cells[1].innerHTML);
            });

            rows.forEach(function(v) {
                tbody.appendChild(v);
            });
        }
   </script>

@endsection

