@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/css/font-awesome.min.css')}}" rel="stylesheet">
@endsection
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
    @endif
    <a href="{{action('\App\Http\Controllers\Admin\ApplicationsController@index')}}" class="btn btn-sm">
        <button type="button" class="btn btn-secondary">
             <strong>ΠΙΣΩ</strong>
        </button>
    </a>
    <div>
        <br>
    </div>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">ΧΡΗΣΤΗΣ</th>
            <th scope="col">ΚΑΤΑΣΤΑΣΗ</th>
            <th scope="col">ΑΡΧΕΙΟ</th>
            <th scope="col">ΠΟΣΟ</th>
            <th scope="col">ΗΜΕΡΟΜΗΝΙΑ ΠΛΗΡΩΜΗΣ</th>
            <th scope="col">ΕΝΕΡΓΕΙΕΣ</th>
        </tr>
        </thead>
        <tbody>
        @foreach($payments as $payment)
            <tr>
                <th scope="row">{{$payment->id}}</th>
                <td>{{$application->user->name}}</td>
                <td>{{$payment->status->label}}</td>
                <td>
                    @if($payment->invoice)
                    <a href="{{route('admin.applications.download-document',['application'=>$application->id,'payment'=>$payment->id])}}">
                        <svg class="c-icon">
                            <use xlink:href="{{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-cloud-download')}}"/>
                        </svg>
                        </a>
                    @endif
                </td>
                <td>{{$payment->schedule->amount}}</td>
                <td>{{!empty($payment->payment_date) ? $payment->payment_date->format('d/m/Y') : ''}}</td>
                <td>
                    @if($payment->invoice && $payment->status->name === 'pending')
                    <div onclick="approvePayment({{$payment->id}})" class="action-buttons">
                        <button class="btn btn-sm btn-success">ACCEPT</button>
                    </div>
                    <div onclick="rejectPayment({{$payment->id}})" class="action-buttons">
                        <button class="btn btn-sm btn-danger">REJECT</button>
                    </div>
                    @endif
                </td>
        @endforeach
    </table>
    {!! $payments->appends(request()->except('page'))->links() !!}
@endsection
@section('bottomAssets')
    <!-- Plugins and scripts required by this view-->
    <script src="{{asset('admin_ui/vendors/@coreui/utils/js/coreui-utils.js')}}"></script>
    <script>

        function approvePayment(paymentId){
            let r = window.confirm('Η πληρωμή θα γίνει αποδεκτή');
            let applicationId = '{{$application->id}}';
            if(r){
                $.ajax({
                    url: '/{{config('app.admin_prefix')}}/applications/'+applicationId+'/payments/'+paymentId+'/accept',
                    type: 'PATCH',
                    success: function(result) {
                        alert(result.message);
                        window.location.reload();
                    }
                });
            }
        }

        function rejectPayment(paymentId){
            let r = window.confirm('Η πληρωμή θα απορριφθεί');
            let applicationId = '{{$application->id}}';
            if(r){
                $.ajax({
                    url: '/{{config('app.admin_prefix')}}/applications/'+applicationId+'/payments/'+paymentId+'/reject',
                    type: 'PATCH',
                    success: function(result) {
                        alert(result.message);
                        window.location.reload();
                    }
                });
            }
        }
    </script>
@endsection

