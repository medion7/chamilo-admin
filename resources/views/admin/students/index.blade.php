@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
    @endif
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
        <div class="col-md-12">
           <h3>Μάθημα: {{$course->title}}</h3>
        </div>
    </div>
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
        <div class="col-md-12">
            <button class="btn btn-primary mb-2" onclick="createCertificates({{$course->id}})">ΕΚΔΟΣΗ ΠΙΣΤΟΠΟΙΗΤΙΚΩΝ</button>
        </div>
        <div class="col-md-3">
            <div style="display: inline-block">
                <label for="search-input">Αναζήτηση</label>
                <input type="text" id="search-input" class="form-control" placeholder="Αναζήτηση"
                       value="{{Request::input('term')}}"/>
            </div>
            <div style="display: inline-block">
                <button class="btn-sm btn-primary" style="display:inline-block;" id="search-button">
                    <i class="cil-search"></i>
                </button>
            </div>
        </div>
    </div>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">ΟΝΟΜΑ</th>
            <th scope="col">ΕΠΙΘΕΤΟ</th>
            <th scope="col">ΠΑΤΡΩΝΥΜΟ</th>
            <th scope="col">EMAIL</th>
            <th scope="col">ΕΓΓΡΑΦΗ</th>
            <th scope="col">ΟΛΟΚΛΗΡΩΣΕ</th>
            <th scope="col">ΠΙΣΤΟΠΟΙΗΤΙΚΟ</th>
        </tr>
        </thead>

        <tbody>
        @foreach($students as $student)
            <tr>
                <th scope="row">{{$student->id}}</th>
                <td>{{$student->name}}</td>
                <td>{{$student->surname}}</td>
                <td>{{$student->dad_name}}</td>
                <td>{{$student->email}}</td>
                <td>{{$student->applications[0]->created_at->format('d/m/Y')}}</td>
                <td>
                    {{$student->applications[0]->certified ? 'ΝΑΙ' : 'ΟΧΙ'}}
                </td>
                <td>
                    {{$student->applications[0]->certificate ? $student->applications[0]->certificate->created_at->format('d/m/Y') : 'ΟΧΙ'}}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $students->appends(request()->except('page'))->links() !!}
@endsection
@section('bottomAssets')
    <!-- Plugins and scripts required by this view-->
    <script src="{{asset('admin_ui/vendors/@coreui/utils/js/coreui-utils.js')}}"></script>
    <script type="text/javascript">
        $("#search-button").on('click', function () {
            let searchInputValue = $("#search-input").val();
            var url = new URL(window.location.href);
            var search_params = url.searchParams;
            search_params.set('term', searchInputValue);
            url.search = search_params.toString();
            window.location.href = url.toString();
        });

        function createCertificates(courseId) {
            let r = window.confirm('Πρόκειται να δημιουργηθούν και να αποσταλούν όλα τα πιστοποιητικά στους μαθητές που έχουν ολοκληρώσει το μάθημα. Είστε σίγουρος/η ;')
            if (r) {
                $.ajax({
                    url: '/{{config('app.admin_prefix')}}/courses/' + courseId+'/create-certificates',
                    type: 'POST',
                    success: function (result) {
                        alert(result.message);
                        window.location.reload();
                    }
                });
            }
        }
    </script>
@endsection

