@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/vendors/@coreui/chartjs/css/coreui-chartjs.css')}}" rel="stylesheet">
@endsection
@section('content')
    <div class="card">
        <div class="card-header">Προσθήκη Χρήστη</div>
        <form class="form-horizontal" action="{{action('\App\Http\Controllers\Admin\UsersController@store')}}"
              method="post">
            <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                {{csrf_field()}}
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Όνομα</label>
                    <div class="col-md-9">
                        <input class="form-control" id="" type="text" name="name" placeholder="" autocomplete=""
                               value="{{old('name')}}"><span class="help-block">Εισάγετε το όνομα σας</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Επίθετο</label>
                    <div class="col-md-9">
                        <input class="form-control" id="" type="text" name="surname" placeholder="" autocomplete=""
                               value="{{old('surname')}}"><span class="help-block">Εισάγετε το επίθετο σας</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Πατρώνυμο</label>
                    <div class="col-md-9">
                        <input class="form-control" id="" type="text" name="dad_name" placeholder="" autocomplete=""
                               value="{{old('dad_name')}}"><span class="help-block">Εισάγετε το πατρώνυμο σας</span>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="hf-email">Email</label>
                    <div class="col-md-9">
                        <input class="form-control" id="" type="email" name="email" placeholder="" autocomplete="email"
                               value="{{old('email')}}"><span class="help-block">Εισάγετε το email σας</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Επαγγελματική/Εκπαιδευτική Ειδικότητα</label>
                    <div class="col-md-9">
                        <input class="form-control" id="" type="text" name="specialty" placeholder="" autocomplete=""
                               value="{{old('specialty')}}"><span class="help-block">Εισάγετε την ειδικότητα σας</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label" for="password">Κωδικός</label>
                    <div class="col-md-9">
                        <input class="form-control" id="" type="password" name="password" placeholder=""
                               autocomplete="current-password"><span class="help-block">Εισάγετε τον κωδικό σας</span>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-md-3 col-form-label">Επιλογή Ρόλου:</label>
                    <div class="col-md-9">
                        <select name="role_id" class="form-control">
                            @foreach($roles as $role)
                                <option
                                    value="{{$role->id}}" {{old('role_id') == $role->id ? 'selected' : ''}}>{{$role->label}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="btn btn-primary">ΠΡΟΣΘΗΚΗ</button>
                <a href="{{action('\App\Http\Controllers\Admin\UsersController@index')}}" class="btn btn-danger"
                   type="">ΠΙΣΩ</a>
            </div>
        </form>
    </div>
@endsection
