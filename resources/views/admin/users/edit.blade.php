@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('headAssets')
    <link href="{{asset('admin_ui/vendors/@coreui/chartjs/css/coreui-chartjs.css')}}" rel="stylesheet">
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
        <br />
        <h3>Επεξεργασία Χρήστη</h3>
        <br />
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form method="post" action="{{action('\App\Http\Controllers\Admin\UsersController@update', $user->id)}}">
            @csrf
            @method('patch')
            <label class="col-md-3 col-form-label" >Όνομα</label>
            <div class="form-group">
                <input type="text" name="name" class="form-control" value="{{old('name',$user->name)}}" placeholder=""/>
            </div>

            <label class="col-md-3 col-form-label" >Επίθετο</label>
            <div class="form-group">
                <input type="text" name="surname" class="form-control" value="{{old('surname',$user->surname)}}" placeholder=""/>
            </div>

            <label class="col-md-3 col-form-label" >Πατρώνυμο</label>
            <div class="form-group">
                <input type="text" name="dad_name" class="form-control" value="{{old('dad_name',$user->dad_name)}}" placeholder=""/>
            </div>

            <label class="col-md-3 col-form-label" >Email</label>
            <div class="form-group">
                <input type="text" name="email" class="form-control" value="{{old('email',$user->email)}}" placeholder=""/>
            </div>

            <label class="col-md-3 col-form-label" >Επαγγελματική/Εκπαιδευτική Ειδικότητα</label>
            <div class="form-group">
                <input type="text" name="specialty" class="form-control" value="{{old('specialty',$user->specialty)}}" placeholder=""/>
            </div>
            <label class="col-md-3 col-form-label" >Κωδικός</label>
            <div class="form-group">
                <input type="password" name="password" class="form-control" value="" placeholder=""/>
            </div>
            <div class="form-group row">
                <label class="col-md-3 col-form-label">Επιλογή Ρόλου:</label>
                <div class="col-md-9">
                    <select name="role_id" class="form-control">
                        @foreach($roles as $role)
                            <option value="{{$role->id}}"
                                {{old('role_id',$user->roles()->first()->id) == $role->id ? 'selected' : ''}}
                            >{{$role->label}}
                            </option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary" value="ΑΠΟΘΗΚΕΥΣΗ" />
                <a href="{{action('\App\Http\Controllers\Admin\UsersController@index')}}"
                   class="btn btn-danger" type="">ΠΙΣΩ</a>
            </div>
        </form>
    </div>
</div>

@endsection
