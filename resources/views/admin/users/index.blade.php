@extends('admin.layouts.app',['pageName'=>'Πίνακας Ελέγχου'])
@section('content')
    @if (session()->has('success'))
        <div class="alert alert-success">
            {{session()->get('success')}}
        </div>
    @endif
    <a href="{{action('\App\Http\Controllers\Admin\UsersController@create')}}" class="btn btn-primary mb-2">+ ΠΡΟΣΘΗΚΗ
        ΧΡΗΣΤΗ</a>
    <div class="row" style="margin-top:20px; margin-bottom:20px;">
        <div class="col-md-3">
            <div style="display: inline-block">
                <label for="search-input">Αναζήτηση</label>
                <input type="text" id="search-input" class="form-control" placeholder="Αναζήτηση"
                       value="{{Request::input('term')}}"/>
            </div>
            <div style="display: inline-block">
                <button class="btn-sm btn-primary" style="display:inline-block;" id="search-button">
                    <i class="cil-search"></i>
                </button>
            </div>
        </div>
    </div>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">ΟΝΟΜΑ</th>
            <th scope="col">ΕΠΙΘΕΤΟ</th>
            <th scope="col">ΠΑΤΡΩΝΥΜΟ</th>
            <th scope="col">EMAIL</th>
            <th scope="col">ΕΙΔΙΚΟΤΗΤΑ</th>
            <th scope="col">ΕΓΓΡΑΦΗ</th>
            <th scope="col">ΡΟΛΟΙ</th>
            <th scope="col">ΕΝΕΡΓΕΙΕΣ</th>
        </tr>
        </thead>

        <tbody>
        @foreach($users as $user)
            <tr>
                <th scope="row">{{$user->id}}</th>
                <td>{{$user->name}}</td>
                <td>{{$user->surname}}</td>
                <td>{{$user->dad_name}}</td>
                <td>{{$user->email}}</td>
                <td>{{$user->specialty}}</td>
                <td>{{$user->created_at->format('d/m/Y')}}</td>
                <td>
                    @foreach($user->roles as $role)
                        {{$role->label}}@if (!$loop->last), @endif
                    @endforeach
                </td>
                <td>
                    <a href="{{action('\App\Http\Controllers\Admin\UsersController@edit',$user->id)}}">
                        <svg class="c-icon">
                            <use
                                xlink:href=" {{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-color-border')}}"/>
                        </svg>
                    </a>
                    <div onclick="deleteUser({{$user->id}})" class="action-buttons">
                        <svg class="c-icon">
                            <use
                                xlink:href="{{asset('admin_ui/vendors/@coreui/icons/svg/free.svg#cil-user-unfollow')}}"/>
                        </svg>
                    </div>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! $users->appends(request()->except('page'))->links() !!}
@endsection
@section('bottomAssets')
    <!-- Plugins and scripts required by this view-->
    <script src="{{asset('admin_ui/vendors/@coreui/utils/js/coreui-utils.js')}}"></script>
    <script type="text/javascript">

        $("#search-button").on('click', function () {
            let searchInputValue = $("#search-input").val();
            var url = new URL(window.location.href);
            var search_params = url.searchParams;
            search_params.set('term', searchInputValue);
            url.search = search_params.toString();
            window.location.href = url.toString();
        });


        function deleteUser(userId) {
            let r = window.confirm('Ο Χρήστης πρόκειται να ΔΙΑΓΡΑΦΕΙ! ΕΙΣΤΕ ΣΙΓΟΥΡΟΣ ??')
            if (r) {
                $.ajax({
                    url: '{{config('app.admin_prefix')}}/users/' + userId,
                    type: 'DELETE',
                    success: function (result) {
                        alert(result.message);
                        window.location.reload();
                    }
                });
            }
        }
    </script>
@endsection

