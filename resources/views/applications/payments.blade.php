<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="overflow-hidden sm:rounded-lg">
                @if(session()->has('status'))
                    <div class="bg-yellow-200 text-sm rounded-md p-3 mb-4">
                        {{session()->get('status')}}
                    </div>
                @endif
                @if ($errors->any())
                    <div class="bg-red-200 text-sm rounded-md p-3 mb-4">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <div class="text-2xl">
                    Μάθημα: {{$application->course->title}}
                </div>
                <div class="grid md:grid-cols-1">
                    @foreach($payments as $payment)
                        <div class="my-2 p-6 bg-white rounded">
                            <div class="flex">
                                <div class="text-lg leading-7 font-semibold">
                                    Ημερομηνία πληρωμής: {{$payment->schedule->payment_date->format('d/m/Y')}}
                                </div>
                            </div>
                            <div class="mt-2 text-gray-600 dark:text-gray-400">
                                Τιμή: {{$payment->schedule->amount}} ευρώ
                            </div>
                            @if($payment->status->name === 'approved')
                                <div class="mt-2 text-green-600 dark:text-gray-400 font-bold">
                                    Πληρώθηκε
                                </div>
                            @endif
                            @if($payment->status->name === 'pending')
                                @if($payment->invoice)
                                    <div class="mt-2 text-green-600 dark:text-gray-400 font-bold">
                                        Δεν έχει εγκριθεί
                                    </div>
                                @endif
                            @endif
                            @if($payment->status->name === 'rejected' || ($payment->status->name === 'pending' && !$payment->invoice))
                                <div class="mt-2 dark:text-gray-400 text-sm font-bold">
                                    <form class="form-horizontal"
                                          enctype="multipart/form-data"
                                          action="{{route('upload-payment-document',['payment' => $payment->id])}}"
                                          method="post">
                                        @csrf
                                        <div class="my-6">
                                            <input type="file" class="block" name="document">
                                            <div class="my-2 text-sm italic text-gray-600">
                                                PDF μέχρι 4mb.
                                            </div>
                                        </div>
                                        <x-jet-button>Ανέβασμα Αποδεικτικού</x-jet-button>
                                    </form>
                                </div>
                            @endif
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
