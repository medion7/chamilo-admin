<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">

            <div class="my-4 px-2">
                <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                    {{ __('Αίτημα Παρακολούθησης') }}
                </h2>
            </div>
            <div class="overflow-hidden sm:rounded-lg">
                @if(session()->has('status'))
                    <div class="bg-yellow-200 text-sm rounded-md p-3 my-2">
                        {{session()->get('status')}}
                    </div>
                @endif
                <div class="my-2 p-6 bg-white rounded">
                    @if(!empty($course->photo_path))
                        <div class="mb-4">
                            <img alt="course image" class="mw-100 m-auto course-image-lg"
                                 src="{{Storage::disk('public')->url($course->photo_path)}}">
                        </div>
                    @endif
                    <div class="flex">
                        <div class="text-lg leading-7 font-semibold">{{$course->title}}</div>
                    </div>
                    <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                        {!! $course->body !!}
                    </div>
                    <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                        Τιμή: {{$course->price}} ευρώ
                    </div>
                    <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm">
                        Εγγραφές: {{$course->course_start->format('d/m/Y')}}
                        εώς {{$course->registration_end->format('d/m/Y')}}
                    </div>
                    <div class="mt-2 text-gray-600 dark:text-gray-400 text-sm fa-bold">
                        Μαθήματα: {{$course->course_end->format('d/m/Y')}}
                        εώς {{$course->registration_end->format('d/m/Y')}}
                    </div>
                    @if(!Auth::user()->hasAppliedForCourse($course->id))
                        <div class="my-2">
                            <form class="form-horizontal"
                                  action="{{route('apply-for-course',['course'=>$course->id])}}"
                                  method="post">
                                @csrf
                                <div class="block my-4">
                                    <label for="requires_invoice" class="flex items-center">
                                        <input id="requires_invoice" type="checkbox" class="form-checkbox" name="requires_invoice">
                                        <span class="ml-2 text-sm text-gray-600">{{ __('Τιμολόγιο') }}</span>
                                    </label>
                                </div>
                                <x-jet-button>Αίτηση</x-jet-button>
                            </form>
                        </div>
                    @else
                        @if(!Auth::user()->hasApprovedApplicationForCourse($course->id))
                            <div class="mt-2 text-yellow-600 dark:text-gray-400 text-sm font-bold">
                                Έχετε ήδη κάνει αίτηση για αυτό το μάθημα
                            </div>
                        @else
                            @if(Auth::user()->hasCertificate($course->id))
                                <div class="mt-2 text-green-600 dark:text-gray-400 text-sm font-bold">
                                    Έχετε ήδη ολοκληρώσει αυτό το μάθημα
                                </div>
                            @else
                                <div class="mt-2 text-green-600 dark:text-gray-400 text-sm font-bold">
                                    Έχετε ήδη πρόσβαση σε αυτό το μάθημα
                                </div>
                            @endif
                            <div class="my-2">
                                @if(Auth::user()->hasCertificate($course->id))
                                    <a href="{{route('download-certificate',['application'=>Auth::user()->approvedApplicationForCourse($course->id)->id])}}">
                                        <x-jet-button>Κατεβασμα Πιστοποιητικου</x-jet-button>
                                    </a>
                                @endif
                                <a href="{{route('payments',['application'=>Auth::user()->approvedApplicationForCourse($course->id)->id])}}">
                                    <x-jet-button>Πληρωμες</x-jet-button>
                                </a>
                            </div>
                        @endif
                    @endif
                </div>
            </div>
        </div>
    </div>
    </div>
</x-app-layout>
