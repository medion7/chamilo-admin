<x-guest-layout>
    <x-jet-authentication-card>
        <x-slot name="logo">
            <a href="{{url('/')}}">
                <img src="/img/logo.png" >
            </a>
        </x-slot>
        <x-jet-validation-errors class="mb-4" />
        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="mt-4">
                <x-jet-label for="gender" value="{{ __('Φύλο') }}" />
                <select name="gender" id="gender" class="form-input rounded-md shadow-sm block mt-1 w-full">
                    <option value="male">Άνδρας</option>
                    <option value="female">Γυναίκα</option>
                </select>
            </div>
            <div class="mt-4">
                <x-jet-label for="name" value="{{ __('Όνομα') }}" />
                <x-jet-input id="name" class="block mt-1 w-full" type="text" name="name" :value="old('name')" required autofocus autocomplete="name"  />
                <div class="text-sm my-2 text-gray-500">
                    Κεφαλαίο το πρώτο γράμμα και πεζά τα υπόλοιπα π.χ. Σπύρος
                </div>
            </div>

            <div class="mt-4">
                <x-jet-label for="surname" value="{{ __('Επώνυμο') }}" />
                <x-jet-input id="surname" class="block mt-1 w-full" type="text" name="surname" :value="old('surname')" required autofocus autocomplete="surname" />
                <div class="text-sm my-2 text-gray-500">
                    Κεφαλαίο το πρώτο γράμμα και πεζά τα υπόλοιπα π.χ. Σπύρος
                </div>
            </div>

            <div class="mt-4">
                <x-jet-label for="dad_name" value="{{ __('Πατρώνυμο') }}" />
                <x-jet-input id="dad_name" class="block mt-1 w-full" type="text" name="dad_name" :value="old('dad_name')" required autofocus autocomplete="dad_name" />
                <div class="text-sm my-2 text-gray-500">
                    Κεφαλαίο το πρώτο γράμμα και πεζά τα υπόλοιπα π.χ. Σπύρος
                </div>
            </div>

            <div class="mt-4">
                <x-jet-label for="email" value="{{ __('Ηλεκτρονική Διεύθυνση') }}" />
                <x-jet-input id="email" class="block mt-1 w-full" type="email" name="email" :value="old('email')" required />
            </div>

            <div class="mt-4">
                <x-jet-label for="dad_name" value="{{ __('Επαγγελματική/Εκπαιδευτική Ειδικότητα') }}" />
                <x-jet-input id="specialty" class="block mt-1 w-full" type="text" name="specialty" :value="old('specialty')" required autofocus autocomplete="specialty" />
                <div class="text-sm my-2 text-gray-500">
                    Αν έχετε επίσημη Επαγγελματική ειδικότητα, αναφέρατε ποια είναι π.χ. ΠΕ70 Δάσκαλος
                </div>
            </div>

            <div class="mt-4">
                <x-jet-label for="password" value="{{ __('Κωδικός πρόσβασης') }}" />
                <x-jet-input id="password" class="block mt-1 w-full" type="password" name="password" required autocomplete="new-password" />
            </div>

            <div class="mt-4">
                <x-jet-label for="password_confirmation" value="{{ __('Επιβεβαίωση κωδικού πρόσβασης') }}" />
                <x-jet-input id="password_confirmation" class="block mt-1 w-full" type="password" name="password_confirmation" required autocomplete="new-password" />
            </div>

            <div class="flex items-center justify-end mt-4">
                <a class="underline text-sm text-gray-600 hover:text-gray-900" href="{{ route('login') }}">
                    {{ __('Ήδη εγγεγραμμένος;') }}
                </a>

                <x-jet-button class="ml-4">
                    {{ __('Εγγραφή') }}
                </x-jet-button>
            </div>
        </form>
    </x-jet-authentication-card>
</x-guest-layout>
