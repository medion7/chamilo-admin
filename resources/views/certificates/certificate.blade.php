<style>
    body { font-family: DejaVu Sans, sans-serif; }
</style>
<img src="{{asset('img/logo_sm.png')}}" alt="logo" style="width:100px; float:left;">
<h2 style="text-align: center">ΠΑΝΕΠΙΣΤΗΜΙΟ ΚΡΗΤΗΣ ΣΧΟΛΗ ΕΠΣΤΗΜΩΝ ΑΓΩΓΗΣ ΠΑΙΔΑΓΩΓΙΚΟ ΤΜΗΜΑ ΔΗΜΟΤΙΚΗΣ ΕΚΠΑΙΔΕΥΣΗΣ Εργαστήριο Προηγμένων Μαθησιακών Τεχνολογιών στην Δια Βίου και Εξ Αποστάσεως Εκπαίδευση (Ε.ΔΙ.Β.Ε.Α)</h2>
<p style="text-align: center; letter-spacing: 5px; font-weight: normal; font-size: 22px;">ΒΕΒΑΙΩΣΗ ΕΠΙΤΥΧΟΥΣ ΟΛΟΚΛΗΡΩΣΗΣ</p>
<p>Βεβαιώνεται ότι {{$student->gender === 'male' ? 'ο' : 'η'}}  {{$student->surname}} {{$student->name}} (Πατρώνυμο: {{$student->dad_name}}, ολοκλήρωσε επιτυχώς το εξ Αποστάσεως επιμορφωτικό πρόγραμμα του Εργαστηρίου Δια Βίου και Εξ Αποστάσεως Εκπαίδευσης του Παιδαγωγικού Τμήματος Δημοτικής Εκπαίδευσης του Πανεπιστημίου Κρήτης, με θέμα: «{{$application->course->title}}».</p>
<p>{!! $application->course->certificate_description !!}</p>
<p>Ρέθυμνο, {{Carbon\Carbon::now()->format('d/m/Y')}} <br>
    Ο Επιστημονικός Υπεύθυνος του Προγράμματος
</p>
<p>
    <img src="{{asset('img/stamp.png')}}" alt="logo" style="width:150px;">
</p>
<p>
    <strong>Παναγιώτης Σ. Αναστασιάδης <br>
        Καθηγητής Πανεπιστημίου Κρήτης</strong>
</p>
