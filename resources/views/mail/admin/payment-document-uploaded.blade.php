<h2>Νέα πληρωμή μαθητή</h2>

<p><strong>Μάθημα:</strong> {{$payment->application->course->title}}</p>
<p><strong>Όνομα μαθητή:</strong> {{$payment->application->user->name}}</p>
<p><strong>Email μαθητή:</strong> {{$payment->application->user->email}}</p>

<a href="{{route('admin.applications.index-payments',['application' => $payment->application])}}">Πληρωμές</a>
