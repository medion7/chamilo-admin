<h2>Η αίτηση σας έγινε αποδεκτή</h2>
<p><strong>Μάθημα:</strong> {{$application->course->title}}</p>
<p><strong>Όνομα μαθητή:</strong> {{$application->user->name}}</p>
<p><strong>Email μαθητή:</strong> {{$application->user->email}}</p>

@if(!$application->course->free)
<p><a href="{{route('payments',['application' => $application->id])}}">Διαχείριση Πληρωμών</a></p>
@endif
