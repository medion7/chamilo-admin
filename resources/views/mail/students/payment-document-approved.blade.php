<h2>Η πληρωμή σας έγινε αποδεκτή</h2>
<p><strong>Μάθημα:</strong> {{$payment->application->course->title}}</p>
<p><strong>Όνομα μαθητή:</strong> {{$payment->application->user->name}}</p>
<p><strong>Email μαθητή:</strong> {{$payment->application->user->email}}</p>


<p><a href="{{route('payments',['application' => $payment->application->id])}}">Διαχείριση Πληρωμών</a></p>
