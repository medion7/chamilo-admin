<x-jet-action-section>
    <x-slot name="title">
        {{ __('Έλεγχος ταυτότητας δύο παραγόντων') }}
    </x-slot>

    <x-slot name="description">
        {{ __('Προσθέστε επιπλέον ασφάλεια στον λογαριασμό σας χρησιμοποιώντας έλεγχο ταυτότητας δύο παραγόντων.') }}
    </x-slot>

    <x-slot name="content">
        <h3 class="text-lg font-medium text-gray-900">
            @if ($this->enabled)
                {{ __('Έχετε ενεργοποιήσει τον έλεγχο ταυτότητας δύο παραγόντων.') }}
            @else
                {{ __('Δεν έχετε ενεργοποιήσει τον έλεγχο ταυτότητας δύο παραγόντων.') }}
            @endif
        </h3>

        <div class="mt-3 max-w-xl text-sm text-gray-600">
            <p>
                {{ __('Όταν είναι ενεργοποιημένος ο έλεγχος ταυτότητας δύο παραγόντων, θα σας ζητηθεί ένα ασφαλές, τυχαίο διακριτικό κατά τον έλεγχο ταυτότητας. Μπορείτε να ανακτήσετε αυτό το διακριτικό από την εφαρμογή Επαληθευτής Google του τηλεφώνου σας.') }}
            </p>
        </div>

        @if ($this->enabled)
            @if ($showingQrCode)
                <div class="mt-4 max-w-xl text-sm text-gray-600">
                    <p class="font-semibold">
                        {{ __('Ο έλεγχος ταυτότητας δύο παραγόντων είναι πλέον ενεργοποιημένος. Σαρώστε τον ακόλουθο κωδικό QR χρησιμοποιώντας την εφαρμογή ελέγχου ταυτότητας του τηλεφώνου σας.') }}
                    </p>
                </div>

                <div class="mt-4">
                    {!! $this->user->twoFactorQrCodeSvg() !!}
                </div>
            @endif

            @if ($showingRecoveryCodes)
                <div class="mt-4 max-w-xl text-sm text-gray-600">
                    <p class="font-semibold">
                        {{ __('Αποθηκεύστε αυτούς τους κωδικούς ανάκτησης σε έναν ασφαλή διαχειριστή κωδικών πρόσβασης. Μπορούν να χρησιμοποιηθούν για την ανάκτηση πρόσβασης στον λογαριασμό σας, εάν χαθεί η συσκευή ελέγχου ταυτότητας δύο παραγόντων.') }}
                    </p>
                </div>

                <div class="grid gap-1 max-w-xl mt-4 px-4 py-4 font-mono text-sm bg-gray-100 rounded-lg">
                    @foreach (json_decode(decrypt($this->user->two_factor_recovery_codes), true) as $code)
                        <div>{{ $code }}</div>
                    @endforeach
                </div>
            @endif
        @endif

        <div class="mt-5">
            @if (! $this->enabled)
                <x-jet-confirms-password wire:then="enableTwoFactorAuthentication">
                    <x-jet-button type="button" wire:loading.attr="disabled">
                        {{ __('Ενεργοποιηση') }}
                    </x-jet-button>
                </x-jet-confirms-password>
            @else
                @if ($showingRecoveryCodes)
                    <x-jet-confirms-password wire:then="regenerateRecoveryCodes">
                        <x-jet-secondary-button class="mr-3">
                            {{ __('Αναγεννηση κωδικων αποκαταστασης') }}
                        </x-jet-secondary-button>
                    </x-jet-confirms-password>
                @else
                    <x-jet-confirms-password wire:then="showRecoveryCodes">
                        <x-jet-secondary-button class="mr-3">
                            {{ __('Εμφάνιση κωδικών ανάκτησης') }}
                        </x-jet-secondary-button>
                    </x-jet-confirms-password>
                @endif

                <x-jet-confirms-password wire:then="disableTwoFactorAuthentication">
                    <x-jet-danger-button wire:loading.attr="disabled">
                        {{ __('Απενεργοποιηση') }}
                    </x-jet-danger-button>
                </x-jet-confirms-password>
            @endif
        </div>
    </x-slot>
</x-jet-action-section>
