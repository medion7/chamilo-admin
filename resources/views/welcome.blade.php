<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>E-LEARNING</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <!-- Styles -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

    @livewireStyles
</head>
<body class="antialiased bg-gray-100 dark:bg-gray-900">
<div style="max-width:100%">
    <img src="/img/head.jpg" style="width:100%">
</div>
<div class="relative flex items-top justify-center bg-gray-100 dark:bg-gray-900 sm:items-center p-6">
    @if (Route::has('login'))
        <div class="fixed top-0 right-0 px-6 py-4 sm:block">
            @auth
                <a href="{{ url('/dashboard') }}" class="text-sm text-gray-200 font-bold">ΑΡΧΙΚΗ ΣΕΛΙΔΑ</a>
            @else
                <a href="{{ route('login') }}" class="text-sm text-gray-200 font-bold">ΣΥΝΔΕΣΗ</a>

                @if (Route::has('register'))
                    <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-200 font-bold">ΕΓΓΡΑΦΗ</a>
                @endif
            @endif
        </div>
    @endif
</div>
<div class="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-4 gap-2">
    @foreach($courses as $course)
        <div class="my-2 p-6 bg-white rounded">
            @if(!empty($course->photo_path))
                <div class="mb-2">
                    <img alt="course image" class="mw-100 m-auto course-image"  src="{{Storage::disk('public')->url($course->photo_path)}}">
                </div>
            @endif
            <div class="text-lg leading-7 font-semibold mb-2">{{$course->title}}</div>
            <div class="text-gray-600 dark:text-gray-400 text-sm mb-4">
                {!! $course->body !!}
            </div>
            <x-jet-button class="max-w-6xl">
                <a href="{{route('register')}}">ΕΓΓΡΑΦΗ</a>
            </x-jet-button>
        </div>
    @endforeach
</div>
</body>
</html>
