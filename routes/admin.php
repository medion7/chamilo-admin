<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group.
|
*/

// Admin dashboard

Route::get('', [App\Http\Controllers\Admin\DashboardController::class,'show'])->name('dashboard');

//Users

Route::name('users.')->prefix('users')->group(function(){
    Route::get('',[App\Http\Controllers\Admin\UsersController::class,'index'])->name('index');
    Route::get('{id}/edit',[App\Http\Controllers\Admin\UsersController::class,'edit'])->name('edit');
    Route::patch('{id}',[App\Http\Controllers\Admin\UsersController::class,'update'])->name('update');
    Route::get('add',[App\Http\Controllers\Admin\UsersController::class,'create'])->name('add');
    Route::post('add',[App\Http\Controllers\Admin\UsersController::class,'store'])->name('store');
    Route::delete('{id}',[App\Http\Controllers\Admin\UsersController::class,'delete'])->name('delete');
});

//Courses

Route::name('courses.')->prefix('courses')->group(function (){

    Route::get('/',[App\Http\Controllers\Admin\CoursesController::class, 'index'])->name('index');
    Route::get('/add',[App\Http\Controllers\Admin\CoursesController::class,'create'])->name('add');
    Route::get('{course}/edit',[App\Http\Controllers\Admin\CoursesController::class,'edit'])->name('edit');
    Route::delete('{course}',[App\Http\Controllers\Admin\CoursesController::class,'delete'])->name('delete');
    Route::patch('/{course}',[App\Http\Controllers\Admin\CoursesController::class,'update'])->name('update');
    Route::post('/{course}/update-image',[App\Http\Controllers\Admin\CoursesController::class,'updateImage'])->name('update-image');
    Route::post('/{course}/delete-image',[App\Http\Controllers\Admin\CoursesController::class,'deleteImage'])->name('delete-image');
    Route::post('add',[App\Http\Controllers\Admin\CoursesController::class,'store'])->name('store');
    Route::get('{course}/students',[App\Http\Controllers\Admin\CoursesController::class,'students'])->name('students');
    Route::post('{course}/create-certificates',[App\Http\Controllers\Admin\CoursesController::class,'createCertificates'])->name('create-subscription');

    //Payment schedules

    Route::name('payment-schedules.')->prefix('{course}/payment_schedules')->group(function(){
        Route::get('',[App\Http\Controllers\Admin\PaymentSchedulesController::class,'index'])->name('index');
        Route::get('create',[App\Http\Controllers\Admin\PaymentSchedulesController::class,'create'])->name('create');
        Route::post('add',[App\Http\Controllers\Admin\PaymentSchedulesController::class,'store'])->name('store');
        Route::get('{payment_schedule}/edit',[App\Http\Controllers\Admin\PaymentSchedulesController::class,'edit'])->name('edit');
        Route::patch('{payment_schedule}',[App\Http\Controllers\Admin\PaymentSchedulesController::class,'update'])->name('update');
        Route::delete('{payment_schedule}',[App\Http\Controllers\Admin\PaymentSchedulesController::class,'delete'])->name('delete');
    });
});

// Applications

Route::name('applications.')->prefix('applications')->group(function (){

    Route::get('',[App\Http\Controllers\Admin\ApplicationsController::class, 'index'])->name('index');
    Route::patch('{application}/accept',[App\Http\Controllers\Admin\ApplicationsController::class,'approve'])->name('accept');
    Route::patch('{application}/reject',[App\Http\Controllers\Admin\ApplicationsController::class,'reject'])->name('reject');
    Route::post('add',[App\Http\Controllers\Admin\ApplicationsController::class,'store'])->name('store');

    // Payments

    Route::get('{application}/payments',[\App\Http\Controllers\Admin\PaymentsController::class,'index'])->name('index-payments');
    Route::patch('{application}/payments/{payment}/accept',[App\Http\Controllers\Admin\PaymentsController::class,'approve'])->name('payment.accept');
    Route::patch('{application}/payments/{payment}/reject',[App\Http\Controllers\Admin\PaymentsController::class,'reject'])->name('payment.reject');
    Route::get('{application}/payments/{payment}/document',[App\Http\Controllers\Admin\PaymentsController::class,'downloadDocument'])->name('download-document');
});






