<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[\App\Http\Controllers\HomeController::class,'welcome'] );

Route::get('/auth/logout',function(){
    Auth::logout();
    return redirect()->back();
});


Route::middleware(['auth:sanctum', 'verified'])->group(function () {
    Route::get('dashboard', [\App\Http\Controllers\HomeController::class, 'dashboard'])->name('dashboard');
    Route::get('sso_callback', [\App\Http\Controllers\SSOController::class, 'sso_callback'])->name('sso-callback');
    Route::get('courses/{course}',[\App\Http\Controllers\ApplicationsController::class,'showCourse'])->name('show-course');
    Route::post('courses/{course}/apply',[\App\Http\Controllers\ApplicationsController::class,'store'])->name('apply-for-course');
    Route::get('applications/{application}/payments',[\App\Http\Controllers\PaymentsController::class,'index'])->middleware('can:view,application')->name('payments');
    Route::post('payments/{payment}',[\App\Http\Controllers\PaymentsController::class,'uploadDocument'])->middleware('can:update,payment')->name('upload-payment-document');
    Route::get('certificates/{application}',[\App\Http\Controllers\ApplicationsController::class,'downloadCertificate'])->name('download-certificate');
});
